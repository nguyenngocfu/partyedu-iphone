//
//  PEFriendViewController.h
//  PartyEDU
//
//  Created by thinhpham on 12/28/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PECustomFriendCell.h"
#import "PEInviteViewController.h"
#import "FriendModel.h"
#import "EventModel.h"

@interface PEFriendViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, CustomFriendDelegate, PopupInviteDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableFriend;
@property (strong, nonatomic) UserModel *userInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imageAvarta;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UILabel *shortDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *favoriteDrinkLabel;

@property (strong, nonatomic) FriendModel *friendInvite;
@property (strong, nonatomic) FriendModel *friendDelete;
@property (strong, nonatomic) EventModel *eventInvite;
@end
