

#import <Foundation/Foundation.h>
#define HEIGHT_IPHONE_5 568
#define HEIGHT_IOS7 20
#define WIDTH_LEFT_SLIDE 273
#define IS_IPHONE   ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds ].size.height == HEIGHT_IPHONE_5)

#define myAppDelegate (PEAppDelegate *) [[UIApplication sharedApplication] delegate]
#define debug(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#define DETECT_OS7    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define DETECT_OS8    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define RGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

#define IS_PORTRAIT ([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationPortrait || [UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationPortraitUpsideDown)
#define IS_LANDSCAPE ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)

#define MBProgressShowing() [MBProgressHUD showHUDAddedTo:self.view animated:YES]
#define MBProgressHiding()  [MBProgressHUD hideAllHUDsForView:self.view animated:YES]

#define NO_INTERNET_CONECTION @"No Internet Connection!"
#define ERROR_CONECTION @"Error connection!"

#define USER_ID @"USER_ID"
#define USER_TOKEN @"USER_TOKEN"
#define USER_INFO @"USER_INFO"
#define USER_EMAIL @"USER_EMAIL"
#define USER_PHONE @"USER_PHONE"
#define USER_RECEIVER @"USER_RECEIVER"
#define CATEGORY @"CATEGORY"
#define SETTINGS_DB_COPIED_STATUS @"SETTINGS_DB_COPIED_STATUS"
#define DATABASE_FILE_NAME @"PartyEDU.sqlite"

#define CELL_TITLE_MAX_WIDTH 225

#define BASE_URL @"http://dynobranding.com/"
#define TIME_OUT 60
#define TIMECOUNT 600

#define POPOVER_CELL_0  @"all"