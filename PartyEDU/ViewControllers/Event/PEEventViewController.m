//
//  PEEventViewController.m
//  PartyEDU
//
//  Created by thinhpham on 12/28/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PEEventViewController.h"
#import "MFSideMenu.h"
#import "Appclient.h"
#import "NSObject+SBJson.h"
#import "EventModel.h"
#import "GoogleMapClient.h"
#import "ActionSheetDatePicker.h"
#import "ActionSheetStringPicker.h"
#import "CategoryModel.h"
#import "PEMyPartiesViewController.h"
#import "PELeftMenuViewController.h"
#import "PESideMenuContainerViewController.h"
#import "PEPartyViewController.h"

@interface PEEventViewController (){
    EventModel *eventModel;
    NSMutableArray *arrayCategory;
    NSMutableArray *arrayState;
    NSDate *dateSelect;
    NSDate *timeSelect;
    NSInteger indexcategory;
    NSInteger indexState;
    int eventID;
    UILabel *placeholderCollege;
    UILabel *placeholderlOtherDetail;
    UIImage *imageSelected;
    
}

@end

@implementation PEEventViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayCategory = [[NSMutableArray alloc]init];
        arrayState = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    _scrollViewEvent.contentSize = CGSizeMake(320, 690);
    _scrollViewEvent.frame = CGRectMake(0, 64, 320, [[UIScreen mainScreen] bounds ].size.height-64);
    
    [self customPlaceholderTextView];
    
    dateSelect = [NSDate date];
    timeSelect = [NSDate date];
    _buttonSelectCategory.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _buttonSelectCategory.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    _buttonSelectDate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _buttonSelectDate.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    _buttonSelectTime.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _buttonSelectTime.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    _buttonSelectState.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _buttonSelectState.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    
    [self getListCategory];
    [self getListState];
    
}
-(void)customPlaceholderTextView{
    placeholderCollege = [[UILabel alloc] initWithFrame:CGRectZero];
    [placeholderCollege setText:@"College Affiliate"];
    [placeholderCollege setBackgroundColor:[UIColor clearColor]];
    placeholderCollege.frame = CGRectMake(6.0, 7, _collegeTextView.frame.size.width - 20.0, 21.0);
    placeholderCollege.numberOfLines = 1;
    placeholderCollege.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    placeholderCollege.backgroundColor=[UIColor clearColor];
    [placeholderCollege setTextColor:[UIColor lightGrayColor]];
    [_collegeTextView addSubview:placeholderCollege];
    
    placeholderlOtherDetail = [[UILabel alloc] initWithFrame:CGRectZero];
    [placeholderlOtherDetail setText:@"Other Details"];
    [placeholderlOtherDetail setBackgroundColor:[UIColor clearColor]];
    placeholderlOtherDetail.frame = CGRectMake(6.0, 7, _collegeTextView.frame.size.width - 20.0, 21.0);
    placeholderlOtherDetail.numberOfLines = 1;
    placeholderlOtherDetail.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    placeholderlOtherDetail.backgroundColor=[UIColor clearColor];
    [placeholderlOtherDetail setTextColor:[UIColor lightGrayColor]];
    [_otherDetailTextView addSubview:placeholderlOtherDetail];
    
}

#pragma mark Custom NavigationBar

- (UIBarButtonItem *)leftMenuBarButtonItem {
    // add menu button
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.frame = CGRectMake(0, 0, 25, 30);
    [menuButton setTitle:@"" forState:UIControlStateNormal];
    [menuButton setImage:[UIImage imageNamed:@"button_Menu.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:menuButton];
}
- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"icon_Location.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}
- (void)rightSideMenuButtonPressed:(id)sender {
    // Push to Myparties view
    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
    PEPartyViewController *parties = [[PEPartyViewController alloc]initWithNibName:@"PEPartyViewController" bundle:nil];
    [parties getCurrentLocation];
    [[slideMenu centerViewController] pushViewController:parties animated:NO];
}

#pragma mark getListCategory
- (void)getListCategory{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getListCategoryWithsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            
            NSArray *arrayCat = [results objectForKey:@"details"];
            for (NSDictionary *dict in arrayCat){
                CategoryModel *category = [[CategoryModel alloc] initWithDictionary:dict];
                if ([category.categoryName isEqualToString:@"Featured Events"]) {
                    continue;
                }
                [arrayCategory addObject:category];
            }
            
            
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}


#pragma mark getListState
- (void)getListState{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
//    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getListStateWithsuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
//        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            
            NSArray *arrayCat = [results objectForKey:@"details"];
            for (NSDictionary *dict in arrayCat){
                [arrayState addObject:[dict objectForKey:@"state_name"]];
            }
            
            
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
//        MBProgressHiding();
    }];
    
}



#pragma mark createEvent
- (void)createNewEvent{
    
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client createNewEvent:eventModel success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSDictionary *dictEvent = [results objectForKey:@"details"];
            eventID = [[dictEvent objectForKey:@"event_id"] intValue];
            if (imageSelected) {
                [self uploadImage];
            }else {
                [self createEventSuccess];
            }
            
            
        }
        else {
            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
}
-(void)uploadImage{
    MBProgressShowing();
    NSURL *url=[NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"event", @"mode",
                            @(eventID),@"id",
                            nil];
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"partyedu/api/photo_upload.php" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        NSDate *start = [NSDate date];
        NSTimeInterval timeInterval = [start timeIntervalSince1970];
        NSString *fileName = [NSString stringWithFormat:@"%f.png",timeInterval];
        [formData appendPartWithFileData: UIImageJPEGRepresentation(imageSelected, 0.8) name:@"uploaded_file" fileName:fileName mimeType:@"file"];
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        //        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        MBProgressHiding();
        
        NSDictionary* results = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            [self createEventSuccess];
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        MBProgressHiding();
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
    
    [httpClient enqueueHTTPRequestOperation:operation];
    
}

-(void)createEventSuccess{
    [[Helpers shared] alertPopupStatus:@"Create new event successfully!" title:@"" delegate:nil];
    // Push to Myparties view
    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
    PEMyPartiesViewController *myParties = [[PEMyPartiesViewController alloc]initWithNibName:@"PEMyPartiesViewController" bundle:nil];
    [[slideMenu centerViewController] pushViewController:myParties animated:NO];
    PELeftMenuViewController *leftMenu = (PELeftMenuViewController *)[slideMenu leftMenuViewController];
    leftMenu.rowSelected = 4;
    [leftMenu.tableMenu reloadData];
}

- (void)getLocation{
    MBProgressShowing();
    GoogleMapClient *mapClient = [[GoogleMapClient alloc]initGooglemap];
    NSString * stringAddress = [NSString stringWithFormat:@"%@ %@ %@",_addressTextField.text, _cityTextField.text,[arrayState objectAtIndex:indexState]];
    [mapClient getLocationFromAddress:stringAddress success:^(AFHTTPRequestOperation *operation, id responseObject) {
        MBProgressHiding();
        NSString *status = [responseObject objectForKey:@"status"];
        if ([status isEqualToString:@"OK"]) {
            NSMutableArray *arrayAddress = [responseObject objectForKey:@"results"];
            for (NSDictionary *dict in arrayAddress) {
                
                NSDictionary *latitueDict = [dict objectForKey:@"geometry"];
                NSDictionary *locationDict = [latitueDict objectForKey:@"location"];
                eventModel.latitue  = [locationDict objectForKey:@"lat"];
                eventModel.longtitue  = [locationDict objectForKey:@"lng"];
            }
            [self createNewEvent];
            
        } else {
            [[Helpers shared] alertStatus:@"" title:@"Address is not valid!" delegate:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        MBProgressHiding();
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];

}
#pragma mark TextField delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
}

#pragma mark TextView
-(void) textViewDidChange:(UITextView *)textView
{
    if (textView == _collegeTextView) {
        if(![_collegeTextView hasText]) {
            [_collegeTextView addSubview:placeholderCollege];
        } else if ([[_collegeTextView subviews] containsObject:placeholderCollege]) {
            [placeholderCollege removeFromSuperview];
        }
    } else {
        if(![_otherDetailTextView hasText]) {
            [_otherDetailTextView addSubview:placeholderlOtherDetail];
        } else if ([[_otherDetailTextView subviews] containsObject:placeholderlOtherDetail]) {
            [placeholderlOtherDetail removeFromSuperview];
        }
    }

}

#pragma mark changeDate

-(IBAction)changeDate:(id)sender{
    
    ActionSheetDatePicker *fullDatePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select a date" datePickerMode:UIDatePickerModeDate selectedDate:dateSelect target:self action:@selector(dateWasSelected:element:) origin:sender];
    fullDatePicker.minimumDate = [NSDate date];
    fullDatePicker.hideCancel = YES;
    [fullDatePicker showActionSheetPicker];
}
- (void)dateWasSelected:(NSDate *)selectedDate element:(id)element {
    dateSelect = selectedDate;
    
    [_buttonSelectDate setTitle:[[Helpers shared] convertDateFromDate:dateSelect] forState:UIControlStateNormal];
    [_buttonSelectDate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

#pragma mark changeTime

-(IBAction)changeTime:(id)sender {
    
//    NSInteger minuteInterval = 5;
    //clamp date
//    NSInteger referenceTimeInterval = (NSInteger)[timeSelect timeIntervalSinceReferenceDate];
//    NSInteger remainingSeconds = referenceTimeInterval % (minuteInterval *60);
//    NSInteger timeRoundedTo5Minutes = referenceTimeInterval - remainingSeconds;
//    if(remainingSeconds>((minuteInterval*60)/2)) {/// round up
//        timeRoundedTo5Minutes = referenceTimeInterval +((minuteInterval*60)-remainingSeconds);
//    }
    
//    timeSelect = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)referenceTimeInterval];
    
    ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select a time" datePickerMode:UIDatePickerModeTime selectedDate:timeSelect target:self action:@selector(timeWasSelected:element:) origin:sender];
    datePicker.minimumDate = [NSDate date];
//    datePicker.minuteInterval = minuteInterval;
    [datePicker showActionSheetPicker];
}

-(void)timeWasSelected:(NSDate *)selectedTime element:(id)element {
    timeSelect = selectedTime;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    [_buttonSelectTime setTitle:[dateFormatter stringFromDate:selectedTime] forState:UIControlStateNormal];
    [_buttonSelectTime setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}
#pragma mark changeCategory

- (IBAction)changeCategory:(id)sender {
    NSMutableArray *arrayCategoryName = [[NSMutableArray alloc] init];
    for (CategoryModel *categoryModel in arrayCategory) {
        [arrayCategoryName addObject:categoryModel.categoryName];
    }
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select a Category" rows:arrayCategoryName initialSelection:indexcategory doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        indexcategory = selectedIndex;
        [_buttonSelectCategory setTitle:[[arrayCategory objectAtIndex:indexcategory] categoryName] forState:UIControlStateNormal];
        [_buttonSelectCategory setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        NSLog(@"cancel button click");
    } origin:sender];
    
}

#pragma mark changeCategory

- (IBAction)changeState:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select a State" rows:arrayState initialSelection:indexState doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        indexState = selectedIndex;
        [_buttonSelectState setTitle:[arrayState objectAtIndex:indexState] forState:UIControlStateNormal];
        [_buttonSelectState setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        NSLog(@"cancel button click");
    } origin:sender];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonGoClick:(id)sender {
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    eventModel = [[EventModel alloc]init];
    eventModel.userID = (int)[[Helpers shared] getIntegerForKey:USER_ID];
    eventModel.eventName = _partyNameTextField.text;
    eventModel.categoryID = [[arrayCategory objectAtIndex:indexcategory] categoryID];
    eventModel.eventDate = [[Helpers shared] convertDateFromDate:dateSelect];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    eventModel.eventTime = [dateFormatter stringFromDate:timeSelect];
    eventModel.address = _addressTextField.text;
    eventModel.city = _cityTextField.text;
    eventModel.stateEvent = [arrayState objectAtIndex:indexState];
    eventModel.zip_code = _zipCodeTextField.text;
    eventModel.cover = _coverTextField.text;
    eventModel.collage = _collegeTextView.text;
    eventModel.otherDetail = _otherDetailTextView.text;
    
    [self getLocation];
}



#pragma mark Image pickerviewcontroller
- (IBAction)uploadPhotoClick:(id)sender {
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Upload photo"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Take Photo", @"Existing Photo", nil];
    [actionSheet showInView:self.view];
    actionSheet.delegate = self;
    
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        [self takeCamera:self];
    }else  if (buttonIndex == 1) {
        [self takePhoto:self];
    }
}

-(IBAction)takeCamera:(id)sender{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
    }
}
-(IBAction)takePhoto:(id)sender{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)imageInfo
{
    [self dismissViewControllerAnimated:YES completion:nil];
    imageSelected = [imageInfo valueForKey:UIImagePickerControllerOriginalImage];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
