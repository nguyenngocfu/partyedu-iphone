#import <Foundation/Foundation.h>
#import "UserModel.h"
@interface Helpers : NSObject{
    UIAlertView *alertViewPopup;
}

+ (id)shared;
// set
-(BOOL)saveObject:(id)object forKey:(NSString *)key;
-(BOOL)saveInteger:(NSInteger)i forKey:(NSString *)key;
-(void)saveUserInfo:(NSDictionary *)dictUser;
-(void)saveCategoryList:(NSDictionary *)dictCat;

// get
-(id)getObjectForKey:(NSString*)key;
-(NSString *)getStringForKey:(NSString*)key;
-(NSInteger)getIntegerForKey:(NSString*)key;
-(NSString *)getUserToken;
-(NSDictionary *)getUserInfo;
-(NSDictionary *)getCategoryList;

-(NSString *)getImageFromCategory:(int)index;
-(int)getHeightFromText:(NSString *)text;
-(int)getHeightFromTextDetail:(NSString *)text;

//-(void)registerDeviceToken:(NSString *)linkUrl;
- (BOOL)checkNetworkAvailable;

- (void) alertStatus:(NSString *)msg title:(NSString *)title delegate: (id)delegate;
- (void) alertStatusConfirmDelete:(NSString *)msg title:(NSString *)title delegate: (id)delegate;
- (void) alertPopupStatus:(NSString *)msg title:(NSString *)title delegate: (id)delegate;

- (BOOL)validateEmailWithString:(NSString*)email;

-(NSString *)convertDate:(NSString *) timeInterval;
-(NSString*)convertDateSheetInfo:(NSString*)inputDateString;
-(NSString *)convertDateFromDate:(NSDate *) timeDate;

// SQLiteDB
+ (void)setDidCopyDb;
+ (BOOL)isDbCopied;
+ (NSString*)getDbPath;
+ (void)copyDbToPath;
@end
