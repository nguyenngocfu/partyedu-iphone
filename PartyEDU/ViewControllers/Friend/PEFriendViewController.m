//
//  PEFriendViewController.m
//  PartyEDU
//
//  Created by thinhpham on 12/28/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PEFriendViewController.h"
#import "MFSideMenu.h"
#import "PEPartyViewController.h"
#import "UserModel.h"
#import "FriendModel.h"
#import "AFNetworking.h"
#import "NSObject+SBJson.h"
#import "Appclient.h"
#import "PEDetailFriendViewController.h"
#import "PEDetailViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface PEFriendViewController (){
    NSMutableArray *arrayFriend;
}

@end

@implementation PEFriendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayFriend = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    _userInfo = [[UserModel alloc] initWithDictionary:[[Helpers shared] getUserInfo]];
    _userLabel.text = [NSString stringWithFormat:@"%@ %@",_userInfo.firstName, _userInfo.lastName ];
    _shortDescriptionLabel.text = _userInfo.Description;
    _favoriteDrinkLabel.text = _userInfo.sortDescription;
    [_imageAvarta setImageWithURL:[NSURL URLWithString:_userInfo.avataLink] placeholderImage:[UIImage imageNamed:@"avatar.png"]];
    _imageAvarta.layer.cornerRadius = _imageAvarta.frame.size.width/2;
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(callYourMethod:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.tableFriend addGestureRecognizer:swipeRight];
    
}

- (void)callYourMethod:(UISwipeGestureRecognizer *)recognizer
{
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    [self getListFriend];
}

-(void)getListFriend{
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    [arrayFriend removeAllObjects];
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getFriendWithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                FriendModel *friend = [[FriendModel alloc]initWithDictionary:dict];
                [arrayFriend addObject:friend];
            }
            [_tableFriend reloadData];
            
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}


#pragma mark Custom NavigationBar

- (UIBarButtonItem *)leftMenuBarButtonItem {
    // add menu button
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.frame = CGRectMake(0, 0, 25, 30);
    [menuButton setTitle:@"" forState:UIControlStateNormal];
    [menuButton setImage:[UIImage imageNamed:@"button_Menu.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:menuButton];
}
- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"icon_Location.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}
- (void)rightSideMenuButtonPressed:(id)sender {
    // Push to Myparties view
    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
    PEPartyViewController *parties = [[PEPartyViewController alloc]initWithNibName:@"PEPartyViewController" bundle:nil];
    [parties getCurrentLocation];
    [[slideMenu centerViewController] pushViewController:parties animated:NO];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayFriend.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 80;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Delete cell
    NSLog(@"Deleted row.");
    _friendDelete = [arrayFriend objectAtIndex:indexPath.item];
    [self removeFriend];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifier = @"PECustomFriendCell";
    PECustomFriendCell *cell = (PECustomFriendCell*)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil) {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECustomFriendCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.titleUserName.text = [[arrayFriend objectAtIndex:indexPath.row]friendName];
    [cell.img_avarta setImageWithURL:[NSURL URLWithString:[[arrayFriend objectAtIndex:indexPath.row]friendPhoto]] placeholderImage:[UIImage imageNamed:@"avatar.png"]];
    cell.favoriteDrinkLabel.text = [[arrayFriend objectAtIndex:indexPath.row]favoriteDrink];
    cell.collegeLabel.text = [[arrayFriend objectAtIndex:indexPath.row]college];
    cell.buttonAccept.hidden = YES;
    cell.tag = indexPath.row;
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PEDetailFriendViewController *detailFriend = [[PEDetailFriendViewController alloc] initWithNibName:@"PEDetailFriendViewController" bundle:nil];
    if ([[[arrayFriend objectAtIndex:indexPath.row] block] isEqualToString:@"Block"]) {
        detailFriend.checkFriend = 2;
    } else {
        detailFriend.checkFriend = 1;
    }
    
    detailFriend.friend_id = [[arrayFriend objectAtIndex:indexPath.row] friendID];
    [self.navigationController pushViewController:detailFriend animated:YES];
}
#pragma mark CustomFriendDelegate

-(void) showMessageDetail:(NSInteger) tag{
    PEDetailViewController *detailMessageView = [[PEDetailViewController alloc]initWithNibName:@"PEDetailViewController" bundle:nil];
    detailMessageView.friendInfo = [arrayFriend objectAtIndex:tag];
    
    [self.navigationController pushViewController:detailMessageView animated:YES];
    
}
-(void) showPopupInviteParty:(NSInteger) tag{
    PEInviteViewController *inviteView = [[PEInviteViewController alloc]initWithNibName:@"PEInviteViewController" bundle:nil];
    inviteView.delegate = self;
    _friendInvite = [arrayFriend objectAtIndex:tag];
    [self presentPopupViewController:inviteView animationType:MJPopupViewAnimationFade];
}
- (void)dismissPopup
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

#pragma mark invites party

- (void)inviteParty:(EventModel *)event
{
    [self dismissPopup];
    _eventInvite = event;
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[NSString stringWithFormat:@"Invite %@ to %@",_friendInvite.friendName,_eventInvite.eventName]
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Ok", nil];
    [alertView show];
}
-(void)inviteParty{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client sendInvitePartyWithEventID:_eventInvite.eventID fromFriendID:_friendInvite.friendID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            [[Helpers shared] alertPopupStatus:@"Invitation has been sent successfully" title:@"" delegate:nil];
            
        }
        //        else {
        //            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        //        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}

-(void)removeFriend{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client removeFriend:_friendDelete.removeUrl success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            [[Helpers shared] alertPopupStatus:@"Removed successfully" title:@"" delegate:nil];
            // Reload friend list
            [self getListFriend];
            
        }
        //        else {
        //            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        //        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self inviteParty];
    }
}

@end
