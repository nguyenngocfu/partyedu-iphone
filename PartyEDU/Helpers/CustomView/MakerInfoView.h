//
//  MakerInfoView.h
//  PartyEDU
//
//  Created by Phạm Văn Thịnh on 1/6/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventModel.h"

@interface MakerInfoView : UIView
- (id)initWithEvent:(EventModel*)event;
@end
