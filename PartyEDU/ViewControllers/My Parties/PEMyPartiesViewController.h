//
//  PEMyPartiesViewController.h
//  PartyEDU
//
//  Created by thinhpham on 12/28/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PEMyPartiesViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableParty;
@end
