//
//  PELoginViewController.m
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PELoginViewController.h"
#import "Appclient.h"
#import "NSObject+SBJson.h"
#import "UserModel.h"

@interface PELoginViewController ()

@end

@implementation PELoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _scrollViewLogin.contentSize = CGSizeMake(320, 568);
    _scrollViewLogin.frame = CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds ].size.height);
    
    //check user has been login
    if ((int)[[Helpers shared] getIntegerForKey:USER_ID] >0) {
        _emailTextField.text = [[Helpers shared] getObjectForKey:USER_EMAIL];
        [myAppDelegate autoGetNewMessage];
        [self pushToHomeViewAnimation:NO];
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
    [self.navigationItem setHidesBackButton:YES];
    _passTextField.text = @"";
}

-(void)pushToHomeViewAnimation:(BOOL)animated{
    [myAppDelegate initSlideMenu];
    [self.navigationController pushViewController:[myAppDelegate slideMenu] animated:animated];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonLoginClick:(id)sender {
    [self.view endEditing:YES];
    if ([_emailTextField.text isEqualToString:@""] || [_passTextField.text isEqualToString:@""]) {
        [[Helpers shared] alertStatus:@"Please fill all field!" title:@"" delegate:nil];
        return;
    }
    
    if (![[Helpers shared]validateEmailWithString:_emailTextField.text]) {
        [[Helpers shared] alertStatus:@"Invalid email address!" title:@"" delegate:nil];
        return;
    }

    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client loginWithEmail:_emailTextField.text pass:_passTextField.text success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            
            // save customer_id
            NSMutableDictionary *userInfo = [results objectForKey:@"details"];
            [[Helpers shared] saveInteger:[[userInfo objectForKey:@"customer_id"] intValue] forKey:USER_ID];
            [[Helpers shared] saveObject:[userInfo objectForKey:@"email"] forKey:USER_EMAIL];
            [[Helpers shared]saveUserInfo:userInfo];
            [myAppDelegate autoGetNewMessage];
            [self pushToHomeViewAnimation:YES];
            
        } else if ([[results objectForKey:@"Status"] isEqualToString:@"010"]) {
            [[Helpers shared] alertStatus:@"The information you entered is incorrect." title:@"" delegate:nil];
        } else {
            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
}

- (IBAction)buttonRegisterClick:(id)sender {
    PERegisterViewController *registerView = [[PERegisterViewController alloc]initWithNibName:@"PERegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerView animated:YES];
}

- (IBAction)buttonForgotPasswordClick:(id)sender {
    PEForgotPasswordViewController *forgotPWView = [[PEForgotPasswordViewController
        alloc]initWithNibName:@"PEForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:forgotPWView animated:YES];
}
@end
