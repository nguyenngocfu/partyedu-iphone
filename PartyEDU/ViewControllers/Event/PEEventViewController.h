//
//  PEEventViewController.h
//  PartyEDU
//
//  Created by thinhpham on 12/28/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AbstractActionSheetPicker;
#import "MCustomTextfield.h"

@interface PEEventViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewEvent;
@property (weak, nonatomic) IBOutlet MCustomTextfield *partyNameTextField;
@property (weak, nonatomic) IBOutlet MCustomTextfield *addressTextField;
@property (weak, nonatomic) IBOutlet MCustomTextfield *cityTextField;
@property (weak, nonatomic) IBOutlet MCustomTextfield *zipCodeTextField;
@property (weak, nonatomic) IBOutlet MCustomTextfield *coverTextField;
@property (weak, nonatomic) IBOutlet UITextView *collegeTextView;
@property (weak, nonatomic) IBOutlet UITextView *otherDetailTextView;
//@property (nonatomic, strong) AbstractActionSheetPicker *actionSheetPicker;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectCategory;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectDate;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectTime;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectState;

- (IBAction)buttonGoClick:(id)sender;
- (IBAction)uploadPhotoClick:(id)sender;

-(IBAction)changeDate:(id)sender;
-(IBAction)changeTime:(id)sender;
- (IBAction)changeCategory:(id)sender;
- (IBAction)changeState:(id)sender;

@end
