//
//  CustomTextView.m
//  PartyEDU
//
//  Created by thinhpham on 1/9/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "CustomTextView.h"

@implementation CustomTextView

@synthesize leftMargin;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // default margin left = 10
        leftMargin = 10;
        UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem *flexibleSpaceBarButton = [[UIBarButtonItem alloc]
                                                   initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                   target:nil
                                                   action:nil];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleBordered target:self
                                                                      action:@selector(doneClicked:)];
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpaceBarButton,doneButton, nil]];
        self.inputAccessoryView = keyboardDoneButtonView;
    }
    return self;
}
-(void)customPlaceholderLabel:(NSString *)placeHolder{
    // custom placeholderText
    placeholderLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [placeholderLabel setText:placeHolder];
    [placeholderLabel setBackgroundColor:[UIColor clearColor]];
    CGRect caretRect = [self caretRectForPosition:self.beginningOfDocument];
    placeholderLabel.frame = CGRectMake(caretRect.origin.x, caretRect.origin.y, self.frame.size.width - caretRect.origin.x, 22.0);
    placeholderLabel.numberOfLines = 1;
    placeholderLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    
    placeholderLabel.backgroundColor=[UIColor clearColor];
    [placeholderLabel setTextColor:[UIColor lightGrayColor]];
    if(![self hasText]) {
        [self addSubview:placeholderLabel];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged) name:UITextViewTextDidChangeNotification object:nil];
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self resignFirstResponder];
    //    [self.view endEditing:YES];
}


 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 }


#pragma mark TextView
-(void)textChanged
{
    if(![self hasText]) {
        [self addSubview:placeholderLabel];
    } else if ([[self subviews] containsObject:placeholderLabel]) {
        [placeholderLabel removeFromSuperview];
    }
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return YES;
}


@end
