//
//  YFInputBar.h
//  test
//
//  Created by 杨峰 on 13-11-10.
//  Copyright (c) 2013年 杨峰. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YFInputBar;
@protocol YFInputBarDelegate <NSObject>

-(void)showKeyBoard;
-(void)hiddenKeyBoard;
-(void)addComment;
@end
@interface YFInputBar : UIView<UITextFieldDelegate>


@property(assign,nonatomic)id<YFInputBarDelegate> delegate;

@property(strong,nonatomic)UITextField *textField;
@property(strong,nonatomic)UIView *customViewText;


@property(assign,nonatomic)BOOL resignFirstResponderWhenSend;


@property(assign,nonatomic)CGRect originalFrame;


-(BOOL)resignFirstResponder;
@end
