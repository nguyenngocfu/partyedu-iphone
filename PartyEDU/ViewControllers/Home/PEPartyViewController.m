//
//  PEPartyViewController.m
//  PartyEDU
//
//  Created by Phạm Văn Thịnh on 12/31/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PEPartyViewController.h"
#import "MFSideMenu.h"
#import "PEMyPartiesViewController.h"
#import "PECustomListUserParty.h"
#import "NSObject+SBJson.h"
#import "AFNetworking.h"
#import "Appclient.h"
#import "CustomDetailPartyCell.h"
#import "PECustomMyPartyCell.h"
#import "MakerInfoView.h"
#import "PEDetailPartyViewController.h"

#define heightViewInfo 260
@interface PEPartyViewController (){

    BOOL firstLocationUpdate;
    GMSCoordinateBounds *commonBound;
    CLLocation *myLocation;
    NSArray *arraySelectTime;
    NSArray *arrayValueSelectTime;
    int indexSelectTime;
    int popoverWidth;
}

@end

@implementation PEPartyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arrayEvent = [[NSMutableArray alloc]init];
        arraySelectTime = [NSArray arrayWithObjects:@"all", @"today", @"7 days", @"15 days", @"1 month", nil];
        arrayValueSelectTime = [NSArray arrayWithObjects:@"all", @"today", @"7day", @"15day", @"1month", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _viewCover.frame = CGRectMake(0, 64, 320, [[UIScreen mainScreen] bounds ].size.height-64);
    [self setUpNavigationItemRight];
    if(self.navigationController.viewControllers.count == 1)
        self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    
    UITableView *selectTimeView = [[UITableView alloc] init];
    popoverWidth = 100;
    selectTimeView.frame = CGRectMake(0, 0, popoverWidth, 200);
    selectTimeView.dataSource = self;
    selectTimeView.delegate = self;
    self.tableView = selectTimeView;
    self.tableView.tag = 3;
    self.popover = [DXPopover new];
    
    myLocation = [myAppDelegate mylocation];
}
- (void)viewWillAppear:(BOOL)animated{
    
}

#pragma mark Custom NavigationBar

- (UIBarButtonItem *)leftMenuBarButtonItem {
    // add menu button
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.frame = CGRectMake(0, 0, 25, 30);
    [menuButton setTitle:@"" forState:UIControlStateNormal];
    [menuButton setImage:[UIImage imageNamed:@"button_Menu.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:menuButton];
}
- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}
- (UIBarButtonItem *)rightMenuBarButtonItem:(NSString *) imageString {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:imageString] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}

- (void)setUpNavigationItemRight{
    
    // add button filter following time
    UIButton * rightButtonTime = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButtonTime.frame = CGRectMake(0, 0, 40, 30);
    [rightButtonTime setTitle:@"Filter" forState:UIControlStateNormal];
    [rightButtonTime setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [rightButtonTime addTarget:self action:@selector(filterButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *buttonFilter = [[UIBarButtonItem alloc] initWithCustomView:rightButtonTime];
    UIBarButtonItem *buttonFilterMap;
    
    if (_isShowListEvent) {
        buttonFilterMap = [self rightMenuBarButtonItem:@"icon_Location.png"];
        _mapView.hidden = YES;
    } else {
        buttonFilterMap = [self rightMenuBarButtonItem:@"icon_list.png"];
        
        _mapView.hidden = NO;
    }
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:buttonFilterMap,buttonFilter, nil];
    
    
}
- (void)filterButtonPressed:(id)sender {
    [self showPopover];
}

- (void)rightSideMenuButtonPressed:(id)sender {
    _isShowListEvent = !_isShowListEvent;
    [self setUpNavigationItemRight];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    [self.mapView removeObserver:self forKeyPath:@"myLocation"];
    self.mapView.delegate = nil;
    self.mapView = nil;
}

#pragma mark show filter view
- (void)showPopover
{

    UIBarButtonItem *filterBarItem = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
    
    UIView *theView = filterBarItem.customView;
    if (!theView.superview && [filterBarItem respondsToSelector:@selector(view)]) {
        theView = [filterBarItem performSelector:@selector(view)];
    }
    
    UIView *parentView = theView.superview;
    NSArray *subviews = parentView.subviews;
    
    NSUInteger indexOfView = [subviews indexOfObject:theView];
    NSUInteger subviewCount = subviews.count;
    UIView *button;
    if (subviewCount > 0 && indexOfView != NSNotFound) {
        button = [parentView.subviews objectAtIndex:indexOfView];
    }
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(button.frame), 0);
    [self.popover showAtPoint:startPoint popoverPostion:DXPopoverPositionDown withContentView:self.tableView inView:self.view];
    
    __weak typeof(self)weakSelf = self;
    self.popover.didDismissHandler = ^{
        [weakSelf bounceTargetView:button];
    };
}
- (void)bounceTargetView:(UIView *)targetView
{
    [UIView animateWithDuration:0.1 animations:^{
        targetView.transform = CGAffineTransformMakeScale(0.9, 0.9);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            targetView.transform = CGAffineTransformMakeScale(1.1, 1.1);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                targetView.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                
            }];
        }];
    }];
}

#pragma mark Location Delegate

-(void)getCurrentLocation{
    //Get List of shop near me
    MBProgressHiding();
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    [_arrayEvent removeAllObjects];
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client searchEventWithString:@"" WithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                EventModel *event = [[EventModel alloc]initWithDictionary:dict];
                [_arrayEvent addObject:event];
            }
            [self initMapView];
            
        }
        [_tableListEvent reloadData];
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];

}

#pragma mark getListEvent

-(void)getListEvent{
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    [_arrayEvent removeAllObjects];
    
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getListEventWithCategoryID:_categoryID WithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                EventModel *event = [[EventModel alloc]initWithDictionary:dict];
                [_arrayEvent addObject:event];
            }
            
            
        }
        [_tableListEvent reloadData];
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
        [self initMapView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];

}
#pragma mark searchEvent

- (void)searchEventWith:(NSString *)stringEvent{
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    [_arrayEvent removeAllObjects];
    
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client searchEventWithString:stringEvent WithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                EventModel *event = [[EventModel alloc]initWithDictionary:dict];
                [_arrayEvent addObject:event];
            }
            
        }
        [_tableListEvent reloadData];
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
        [self initMapView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];

}
#pragma mark searchEvent

- (void)searchEventFilterTime{
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    [_arrayEvent removeAllObjects];
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client searchEventWithTime:arrayValueSelectTime[indexSelectTime] WithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                EventModel *event = [[EventModel alloc]initWithDictionary:dict];
                [_arrayEvent addObject:event];
            }
            
        }
        [_tableListEvent reloadData];
        //        else {
        //            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        //        }
        [self initMapView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}


#pragma mark GoogleMap
- (void)initMapView{
//    [self initViewBound];
    dispatch_async(dispatch_get_main_queue(), ^{
        _mapView.myLocationEnabled = YES;
    });
    _mapView.mapType = kGMSTypeNormal;
    _mapView.settings.compassButton = YES;
    _mapView.settings.myLocationButton = YES;
    _mapView.myLocationEnabled = YES;
    [_mapView clear];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:myLocation.coordinate.latitude
                                         longitude:myLocation.coordinate.longitude
                                              zoom:8.0];
    _mapView.camera = camera;
    _mapView.delegate = self;
    
//    GMSCoordinateBounds *bounds= [[GMSCoordinateBounds alloc]init];
    
    for (EventModel *event in _arrayEvent) {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake([event.latitue floatValue],[event.longtitue floatValue]);
        
        GMSMarker *maker = [[GMSMarker alloc] init];
        maker.title = event.eventName;
        maker.icon = [UIImage imageNamed:@"icon_Location.png"];
        maker.position = position;
        maker.map = _mapView;
        maker.userData = event;
//        bounds= [bounds includingCoordinate:position];

    }
//    commonBound  = [commonBound includingBounds:bounds];
    [self drawViewBound];
   
    
}

#pragma mark ViewBound

-(void)initViewBound{
    CLLocationCoordinate2D myPosition = CLLocationCoordinate2DMake(myLocation.coordinate.latitude, myLocation.coordinate.longitude);
    commonBound  = [[GMSCoordinateBounds alloc]initWithCoordinate:myPosition coordinate:myPosition];
}
-(void)drawViewBound{
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:commonBound withPadding:20.0f];
    [_mapView animateWithCameraUpdate:update];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!firstLocationUpdate) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        firstLocationUpdate = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:6];
    }
}
#pragma mark GMSMapView Delegate
-(UIView*)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    MakerInfoView *viewMaker = [[MakerInfoView alloc] initWithEvent:marker.userData];
    
    return viewMaker;
}
-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    return NO;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
    PEDetailPartyViewController *detailView = [[PEDetailPartyViewController alloc]initWithNibName:@"PEDetailPartyViewController" bundle:nil];
    detailView.eventSelected = marker.userData;
    [self.navigationController pushViewController:detailView animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma -mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 3) {
        return arraySelectTime.count;
    }
    return _arrayEvent.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    if (tableView.tag == 3) {
        return 40;
    }
    return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 3) {
        static NSString *cellId = @"cellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        cell.textLabel.text = arraySelectTime[indexPath.row];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        return cell;
    }
    
    static NSString *cellIndentifier1 = @"PECustomMyPartyCell";
    PECustomMyPartyCell *cell = (PECustomMyPartyCell*)[tableView dequeueReusableCellWithIdentifier:cellIndentifier1];
    if (cell == nil) {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECustomMyPartyCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.titleParty.text = [[_arrayEvent objectAtIndex:indexPath.row] eventName];
    cell.titleAddress.text = [NSString stringWithFormat:@"%@ %@ %@",[[_arrayEvent objectAtIndex:indexPath.row] address],[[_arrayEvent objectAtIndex:indexPath.row] city],[[_arrayEvent objectAtIndex:indexPath.row] stateEvent]];
    cell.titleTime.text = [NSString stringWithFormat:@"%@ %@",[[_arrayEvent objectAtIndex:indexPath.row] eventTime],[[_arrayEvent objectAtIndex:indexPath.row] eventDate]];
    [cell.img_avarta setImageWithURL:[NSURL URLWithString:[[_arrayEvent objectAtIndex:indexPath.row] imageEvent]] placeholderImage:[UIImage imageNamed:@"no_image"]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 3) {
        indexSelectTime = (int)indexPath.row;
        [self.popover dismiss];
        [self searchEventFilterTime];
        return;
    }
    
    PEDetailPartyViewController *detailView = [[PEDetailPartyViewController alloc]initWithNibName:@"PEDetailPartyViewController" bundle:nil];
    detailView.eventSelected = [_arrayEvent objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:detailView animated:YES];
    
}
@end
