//
//  DBHelper.h
//  PartyEDU
//
//  Created by thinhpham on 2/21/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "MessageModel.h"

//@class EventArtileModule;
@interface DBHelper : NSObject{
    sqlite3 *database;
    sqlite3_stmt *addStmt;
    sqlite3_stmt *deleteStmt;
    sqlite3_stmt *detailStmt;
    
}

@property(nonatomic, strong) MessageModel *model;

- (void)addNewMessage:(MessageModel*)m;
- (void)deleteMessageWithFriendID:(int)friendID;
- (void)deleteAllMessage;
- (NSMutableArray*)getMessageFromUser: (int)userID;
- (BOOL)hasDataRecordsWithFriendID: (int)friendID;

- (void)closeAll;
- (void)finalizeAdd;
- (void)finalizeDelete;
- (void)finalizeSelect;
- (void)close;

@end
