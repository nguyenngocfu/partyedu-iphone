//
//  GoogleMapClient.h
//  PartyEDU
//
//  Created by Phạm Văn Thịnh on 1/6/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "Appclient.h"
@interface GoogleMapClient : AppClient
- (id)initGooglemap;
- (void) getLocationFromAddress:(NSString *)address success:(SuccessResponse)success failure:(FailureResponse)failure;
@end
