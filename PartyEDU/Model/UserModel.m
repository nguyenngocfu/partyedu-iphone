//
//  UserModel.m
//  PartyEDU
//
//  Created by Pham Van Thinh on 1/20/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

- (id)initWithDictionary:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        _userID = [[dict objectForKey:@"customer_id"] intValue];
        if (_userID == 0) {
            _userID = [[dict objectForKey:@"friend_id"] intValue];
        }
        _email = [dict objectForKey:@"email"];
        _college = [dict objectForKey:@"college"];
        _firstName = [dict objectForKey:@"firstname"];
        if (!_firstName) {
            _firstName = [dict objectForKey:@"firsname"];
        }
        _lastName = [dict objectForKey:@"lastname"];
        if (!_firstName && !_lastName) {
            _firstName = [dict objectForKey:@"name"];
            _lastName = @" ";
        }
        _avataLink = [dict objectForKey:@"photoName"];
        _gender = [dict objectForKey:@"gender"];
        _sortDescription = [dict objectForKey:@"sort_description"];
        _Description = [dict objectForKey:@"description"];
    }
    return self;
}
@end
