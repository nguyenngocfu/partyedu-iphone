//
//  CustomDetailPartyCell.h
//  PartyEDU
//
//  Created by thinhpham on 1/6/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomDetailPartyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabelParty;
@property (weak, nonatomic) IBOutlet UILabel *detailTitleLabelParty;

@end
