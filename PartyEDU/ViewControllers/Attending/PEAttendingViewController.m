//
//  PEAttendingViewController.m
//  PartyEDU
//
//  Created by thinhpham on 12/28/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PEAttendingViewController.h"
#import "MFSideMenu.h"
#import "PECustomMyPartyCell.h"
#import "NSObject+SBJson.h"
#import "Appclient.h"
#import "EventModel.h"
#import "AFNetworking.h"
#import "PEPartyViewController.h"
#import "PEDetailPartyViewController.h"

@interface PEAttendingViewController (){
    NSMutableArray *arrayEvent;
}


@end

@implementation PEAttendingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayEvent = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [self getMyParties];
    
}

#pragma mark getMyParties

-(void)getMyParties{
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    [arrayEvent removeAllObjects];
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getMyAttendingWithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                EventModel *event = [[EventModel alloc]initWithDictionary:dict];
                [arrayEvent addObject:event];
            }
            [_tableAttending reloadData];
            
        } else if ([[results objectForKey:@"Status"] isEqualToString:@"010"]) {
            [[Helpers shared] alertStatus:@"You are not attending any parties"title:@"" delegate:nil];
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}


#pragma mark Custom NavigationBar

- (UIBarButtonItem *)leftMenuBarButtonItem {
    // add menu button
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.frame = CGRectMake(0, 0, 25, 30);
    [menuButton setTitle:@"" forState:UIControlStateNormal];
    [menuButton setImage:[UIImage imageNamed:@"button_Menu.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:menuButton];
}
- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}


- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"icon_Location.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}
- (void)rightSideMenuButtonPressed:(id)sender {
    
    // Push to Myparties view
    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
    PEPartyViewController *parties = [[PEPartyViewController alloc]initWithNibName:@"PEPartyViewController" bundle:nil];
    [parties getCurrentLocation];
    [[slideMenu centerViewController] pushViewController:parties animated:NO];
}
#pragma -mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayEvent.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifier = @"PECustomMyPartyCell";
    PECustomMyPartyCell *cell = (PECustomMyPartyCell*)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil) {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECustomMyPartyCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.titleParty.text = [[arrayEvent objectAtIndex:indexPath.row] eventName];
    cell.titleAddress.text = [NSString stringWithFormat:@"%@ %@ %@",[[arrayEvent objectAtIndex:indexPath.row] address],[[arrayEvent objectAtIndex:indexPath.row] city],[[arrayEvent objectAtIndex:indexPath.row] stateEvent]];
    cell.titleTime.text = [NSString stringWithFormat:@"%@ %@",[[arrayEvent objectAtIndex:indexPath.row] eventTime],[[arrayEvent objectAtIndex:indexPath.row] eventDate]];
    [cell.img_avarta setImageWithURL:[NSURL URLWithString:[[arrayEvent objectAtIndex:indexPath.row] imageEvent]] placeholderImage:[UIImage imageNamed:@"no_image"]];
    cell.img_avarta.layer.cornerRadius = cell.img_avarta.layer.frame.size.width/2;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PEDetailPartyViewController *partyView = [[PEDetailPartyViewController alloc]initWithNibName:@"PEDetailPartyViewController" bundle:nil];
    partyView.eventSelected = [arrayEvent objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:partyView animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
