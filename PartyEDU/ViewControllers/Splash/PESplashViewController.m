//
//  PESplashViewController.m
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PESplashViewController.h"

@interface PESplashViewController ()

@end

@implementation PESplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden = YES;
    [self performSelector:@selector(pushToLoginScreen) withObject:self afterDelay:3];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pushToLoginScreen{
//    [[myAppDelegate slideMenu] addNavigationView];
    PELoginViewController *loginView = [[PELoginViewController alloc]initWithNibName:@"PELoginViewController" bundle:nil];
    
    //check user has been login
    if ((int)[[Helpers shared] getIntegerForKey:USER_ID] >0) {
        [self.navigationController pushViewController:loginView animated:NO];
    } else {
        [self.navigationController pushViewController:loginView animated:YES];
    }
}

@end
