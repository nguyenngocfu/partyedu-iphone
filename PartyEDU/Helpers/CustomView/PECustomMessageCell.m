//
//  PECustomMessageCell.m
//  PartyEDU
//
//  Created by thinhpham on 2/20/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PECustomMessageCell.h"

@implementation PECustomMessageCell

- (void)awakeFromNib {
    // Initialization code
    _avartaImage.layer.cornerRadius = _avartaImage.frame.size.width/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
