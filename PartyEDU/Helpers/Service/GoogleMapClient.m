//
//  GoogleMapClient.m
//  PartyEDU
//
//  Created by Phạm Văn Thịnh on 1/6/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "GoogleMapClient.h"
//#import "AFJSONRequestOperation.h"
NSString *const googleMapBaseURL = @"http://maps.googleapis.com/maps/api/";
@implementation GoogleMapClient

- (id)initGooglemap{
    NSURL* url = [NSURL URLWithString:googleMapBaseURL];
    
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [self setDefaultHeader:@"Accept" value:@"application/json"];
    [self setParameterEncoding:AFFormURLParameterEncoding];
    
    self.debug = NO;
    
    return self;
}

#pragma mark Googlemap Services

- (void) getLocationFromAddress:(NSString *)address success:(SuccessResponse)success failure:(FailureResponse)failure{
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *path = [NSString stringWithFormat:@"geocode/json?sensor=false&address=%@", esc_addr];
    
    [super requestByMethod:METHOD_GET widthPath:path withParameters:Nil success:success failure:failure];
}

@end
