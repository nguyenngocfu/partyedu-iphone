//
//  LocationTracking.h
//  GiftCard
//
//  Created by Pham Van Thinh on 6/25/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@protocol LocationDelegate<NSObject>
@optional;
-(void)getPostCode:(NSString *)postCode andLocation: (CLLocation *)location;
-(void)getLocation:(CLLocation *)location;
-(void)failedGetLocation;
@end;

@interface LocationTracking : CLLocationManager<CLLocationManagerDelegate>
@property (nonatomic, assign) BOOL checkGetPostCode;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSArray *arrayCountry;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (weak, nonatomic) id <LocationDelegate> delegateLocation;
-(void)updateLocation;
-(void)stopUpdatingLocation;
@end
