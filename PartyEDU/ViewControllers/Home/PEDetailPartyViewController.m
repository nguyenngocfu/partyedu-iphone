//
//  PEDetailPartyViewController.m
//  PartyEDU
//
//  Created by thinhpham on 1/20/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PEDetailPartyViewController.h"
#import "PECustomListUserParty.h"
#import "PEAttendingViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "NSObject+SBJson.h"
#import "AFNetworking.h"
#import "Appclient.h"
#import "CustomDetailPartyCell.h"
#import "PECustomMyPartyCell.h"
#import "MakerInfoView.h"
#import "UIImageView+AFNetworking.h"
#import "PECustomFriendCell.h"
#import "PECommentCell.h"
#import "PEPartyViewController.h"
#import "PEDetailFriendViewController.h"
#import "PEMyProfileViewController.h"
#import <MapKit/MapKit.h>

@interface PEDetailPartyViewController (){
    BOOL firstLocationUpdate;
    int userID;
    BOOL checkUserJoined;
    GMSCoordinateBounds *commonBound;
    int checkTypeView;
    NSMutableArray *arrayAttending;
    NSMutableArray *arrayComment;
    YFInputBar *inputBar;
    GMSMapView *mapViewCell;
}

@end

@implementation PEDetailPartyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayAttending = [[NSMutableArray alloc]init];
        arrayComment = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    
    NSDictionary *dictCategory = [[Helpers shared] getCategoryList];
    [_categoryIcon setImageWithURL:[NSURL URLWithString:[dictCategory objectForKey:_eventSelected.categoryName]]];
    
    _categoryLabel.text = _eventSelected.categoryName;
    [_imageDetailParty setImageWithURL:[NSURL URLWithString:_eventSelected.imageEvent] placeholderImage:[UIImage imageNamed:@"no_pic_available.png"]];
    _eventNameLabel.text = _eventSelected.eventName;
    
    userID = (int)[[Helpers shared] getIntegerForKey:USER_ID];
    _viewCover.frame = CGRectMake(0, 64, 320, [[UIScreen mainScreen] bounds ].size.height-64);
    checkTypeView = 1;
    [self customKeyboard];
    [self showViewDetail];
    [self checkUserJoined];
    [self initMapView:_mapView];

}


#pragma mark Custom NavigationBar

- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"icon_Location.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}
- (void)rightSideMenuButtonPressed:(id)sender {
    // Push to Myparties view
    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
    PEPartyViewController *parties = [[PEPartyViewController alloc]initWithNibName:@"PEPartyViewController" bundle:nil];
    [parties getCurrentLocation];
    [[slideMenu centerViewController] pushViewController:parties animated:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
            
    //    [self.mapView removeObserver:self forKeyPath:@"myLocation"];
    _mapView.delegate = nil;
    _mapView = nil;
    mapViewCell = nil;
    
}

#pragma mark GoogleMap
- (void)initMapView:(GMSMapView *)mapView{
    
    dispatch_async(dispatch_get_main_queue(), ^{
       mapView.myLocationEnabled = YES;
    });
    mapView.mapType = kGMSTypeNormal;
    mapView.settings.compassButton = YES;
    mapView.settings.myLocationButton = YES;
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
             
    [self initViewBound:mapView];
             
}
#pragma mark ViewBound
         
-(void)initViewBound:(GMSMapView *)mapView{
    commonBound  = [[GMSCoordinateBounds alloc]init];
    GMSCoordinateBounds *bounds= [[GMSCoordinateBounds alloc]init];
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake([_eventSelected.latitue floatValue],[_eventSelected.longtitue floatValue]);
    
    GMSMarker *maker = [[GMSMarker alloc] init];
    maker.title = _eventSelected.eventName;
    maker.icon = [UIImage imageNamed:@"icon_Location.png"];
    maker.position = position;
    maker.map = mapView;
    maker.userData = _eventSelected;
    bounds= [bounds includingCoordinate:position];
    
    commonBound  = [commonBound includingBounds:bounds];
    [self drawViewBound:mapView];
}
-(void)drawViewBound:(GMSMapView *)mapView{
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:commonBound withPadding:20.0f];
    [mapView animateWithCameraUpdate:update];
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake([_eventSelected.latitue floatValue],[_eventSelected.longtitue floatValue]);
    mapView.camera = [GMSCameraPosition cameraWithTarget:position
                                                     zoom:6];
    
}

//-(void)drawViewBound{
//    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:commonBound withPadding:20.0f];
//    [_mapView animateWithCameraUpdate:update];
//}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!firstLocationUpdate) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        firstLocationUpdate = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:6];
    }
}
         
#pragma mark GMSMapView Delegate
-(UIView*)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    MakerInfoView *viewMaker = [[MakerInfoView alloc] initWithEvent:marker.userData];
    // Check for iOS 6
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // Create an MKMapItem to pass to the Maps app
        // Create an MKMapItem to pass to the Maps app
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:marker.position
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:@"My Place"];
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
    }
    _mapView.hidden = YES;
    return viewMaker;
}
-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    
    
    // Check for iOS 6
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // Create an MKMapItem to pass to the Maps app
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:marker.position
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:@"My Place"];
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
    }

    return YES;
}
         
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    // Check for iOS 6
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // Create an MKMapItem to pass to the Maps app
        // Create an MKMapItem to pass to the Maps app
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:marker.position
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:@"My Place"];
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
    }
    _mapView.hidden = YES;
}



#pragma -mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    switch (tableView.tag) {
        case 1:
            return 6;
            break;
        case 2:
            return arrayAttending.count;
            break;
            
        default:
            return arrayComment.count;
            break;
    }
    return 5;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 55)];
//    view.backgroundColor = RGB(205, 55, 54);
//    UILabel *labelSection = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 320, 35)];
//    labelSection.textAlignment = NSTextAlignmentCenter;
//    labelSection.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:32];
//    labelSection.textColor = [UIColor whiteColor];
//    labelSection.text = [arraySectionTitle objectAtIndex:section];
//    [view addSubview:labelSection];
//    return view;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    switch (tableView.tag) {
        case 1:{
            if (indexPath.row == 5) {
                return 320;
            } else if(indexPath.row == 0){
                return 55;
            } else if(indexPath.row == 4){
                return ([[Helpers shared] getHeightFromTextDetail:_eventSelected.otherDetail]+ 19 +4 );
            } else
                return 45;
        }
            break;
        case 2:
            return 80;
            break;
            
        default:
            return ([[Helpers shared] getHeightFromText:[[arrayComment objectAtIndex:indexPath.row] objectForKey:@"comment"]]+ 30 +10 );
            break;
    }

    return 40;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIndentifier = @"CustomDetailPartyCell";
    if (tableView.tag == 1) {
//        NSLog(@"Row reload data: %i",indexPath.row);
        if(indexPath.row == 5){
            UITableViewCell *cell;
            
//            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
//            }
            mapViewCell = [[GMSMapView alloc]initWithFrame:CGRectMake(0, 0, 320, 320)];
            dispatch_async(dispatch_get_main_queue(), ^{
                mapViewCell.myLocationEnabled = YES;
            });
            mapViewCell.mapType = kGMSTypeNormal;
            [self initViewBound:mapViewCell];
            [cell addSubview:mapViewCell];
            mapViewCell.delegate = self;
            
            return cell;

        }
        // The cell row
        
        CustomDetailPartyCell *cell = (CustomDetailPartyCell*)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
        
//        if (cell == nil) {
            cell =  [[[NSBundle mainBundle] loadNibNamed:@"CustomDetailPartyCell" owner:self options:nil] objectAtIndex:0];
//        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        switch (indexPath.row) {
            case 0:
            {
                cell.titleLabelParty.text = @"Address";
                cell.detailTitleLabelParty.text = [NSString stringWithFormat:@"%@\n%@, %@ %@",_eventSelected.address, _eventSelected.city, _eventSelected.stateEvent, _eventSelected.zip_code];
            }
                break;
            case 1:
            {
                cell.titleLabelParty.text = @"Contact";
                cell.detailTitleLabelParty.text = _eventSelected.userName;
            }
                break;
            case 2:
            {
                cell.titleLabelParty.text = @"Cover";
                cell.detailTitleLabelParty.text = _eventSelected.cover;
            }
                break;
            case 3:
            {
                cell.titleLabelParty.text = @"College Affiliate";
                cell.detailTitleLabelParty.text = _eventSelected.collage;
            }
                
                break;
            default:{
                cell.titleLabelParty.text = @"Other Details";
                cell.detailTitleLabelParty.text = _eventSelected.otherDetail;
            }
                break;
        }
        return cell;

    } else if (tableView.tag == 2){
        PECustomFriendCell *cell = (PECustomFriendCell*)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
        if (cell == nil) {
            cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECustomFriendCell" owner:self options:nil] objectAtIndex:0];
        }
        NSDictionary *dict = [arrayAttending objectAtIndex:indexPath.row];
        cell.titleUserName.text = [dict objectForKey:@"User Name"];
        CGRect frame = cell.titleUserName.frame;
        frame.size.width = 231;
        cell.titleUserName.frame = frame;
        [cell.img_avarta setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"photoName"]] placeholderImage:[UIImage imageNamed:@"avatar.png"]];
        cell.favoriteDrinkLabel.text = [dict objectForKey:@"User Email"];
        cell.collegeLabel.text = @"";
        cell.buttonAccept.hidden = YES;
        cell.buttonInvite.hidden = YES;
        cell.buttonMessage.hidden = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    
    PECommentCell *cell = (PECommentCell*)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil) {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECommentCell" owner:self options:nil] objectAtIndex:0];
    }
    NSDictionary *dict = [arrayComment objectAtIndex:indexPath.row];
    if (dict) {
        cell.userName.text = [dict objectForKey:@"user_name"];
        [cell.avartaUser setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"photoName"]] placeholderImage:[UIImage imageNamed:@"avatar.png"]];
        cell.commentUser.text = [dict objectForKey:@"comment"];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
   
    
    }
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (checkTypeView == 1) {
        if (indexPath.row == 0) {
            _mapView.hidden = NO;
            
        } else if(indexPath.row == 1){
            PEDetailFriendViewController *detailFriend = [[PEDetailFriendViewController alloc] initWithNibName:@"PEDetailFriendViewController" bundle:nil];
            detailFriend.friend_id = _eventSelected.userID;
            [self.navigationController pushViewController:detailFriend animated:YES];
       
        }
    } else if(checkTypeView == 2){
        
        int friend_id = [[[arrayAttending objectAtIndex:indexPath.row] objectForKey:@"User Id"] intValue];
        if (friend_id == userID) {
            PEMyProfileViewController *profileView = [[PEMyProfileViewController alloc]initWithNibName:@"PEMyProfileViewController" bundle:nil];
            [self.navigationController pushViewController:profileView animated:YES];
            
            PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
            PELeftMenuViewController *leftMenu = (PELeftMenuViewController *)[slideMenu leftMenuViewController];
            leftMenu.rowSelected = 0;
            [leftMenu.tableMenu reloadData];
            return;
        }
        PEDetailFriendViewController *detailFriend = [[PEDetailFriendViewController alloc] initWithNibName:@"PEDetailFriendViewController" bundle:nil];
        detailFriend.friend_id = friend_id;
        [self.navigationController pushViewController:detailFriend animated:YES];
        
    }
    
}

#pragma mark join/unJoin Event
- (void) checkUserJoined{
    MBProgressHiding();
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getListUserJoinWithEvent:_eventSelected.eventID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *arrayUser = [results objectForKey:@"details"];
            checkUserJoined = NO;
            for (NSDictionary *dict in arrayUser) {
                if ([[dict objectForKey:@"User Email"] isEqualToString:[[Helpers shared] getStringForKey:USER_EMAIL]] ) {
                    checkUserJoined = YES;
                    break;
                }
            }
            
        } else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        }
        [self setUpButtonJoin];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}
- (IBAction)joinEvent:(id)sender{
    
    MBProgressHiding();
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    
    AppClient *client = [[AppClient alloc]initJSONClient];
    if (checkUserJoined) {
        [client unJoinPartieWithUserID:userID andEventID:_eventSelected.eventID success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSDictionary *results = [responseString JSONValue];
            MBProgressHiding();
            if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
                checkUserJoined = NO;
                [self setUpButtonJoin];
                
            }
//            else {
//                [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Failure: %@",error);
            [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
            MBProgressHiding();
        }];
    } else {
        [client joinPartieWithUserID:userID andEventID:_eventSelected.eventID success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSDictionary *results = [responseString JSONValue];
            MBProgressHiding();
            if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
                checkUserJoined = YES;
                [self setUpButtonJoin];
                // Push to Myparties view
                PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
                [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
                PEAttendingViewController *myParties = [[PEAttendingViewController alloc]initWithNibName:@"PEAttendingViewController" bundle:nil];
                [[slideMenu centerViewController] pushViewController:myParties animated:NO];
                PELeftMenuViewController *leftMenu = (PELeftMenuViewController *)[slideMenu leftMenuViewController];
                leftMenu.rowSelected = 7;
                [leftMenu.tableMenu reloadData];
            }
//            else {
//                [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Failure: %@",error);
            [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
            MBProgressHiding();
        }];
    }
    

}

-(void)setUpButtonJoin{
    NSString *title = @"";
    if (checkUserJoined) {
        title = @"Unjoin";
    } else {
        title = @"Join";
    }
    [_buttonJoin setTitle:title forState:UIControlStateNormal];
}

#pragma mark showViewInfo
- (IBAction)showViewContent:(id)sender {
    checkTypeView = (int)[sender tag];
    switch ([sender tag]) {
        case 1:
            [self showViewDetail];
            break;
        case 2:
            [self showViewAttending];
            break;
            
        default:
            [self showViewComment];
            break;
    }
}
-(void)showViewDetail{
    _viewDetail.hidden = NO;
    _viewAttending.hidden = YES;
    _viewComment.hidden = YES;
    inputBar.hidden = YES;
}
-(void)showViewAttending{
    _viewDetail.hidden = YES;
    _viewAttending.hidden = NO;
    _viewComment.hidden = YES;
    inputBar.hidden = YES;
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    [arrayAttending removeAllObjects];
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getListMemberWithEventID:_eventSelected.eventID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                [arrayAttending addObject:dict];
            }
            
            
        }
        [_tableAttendingView reloadData];
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];

    
}
-(void)showViewComment{
    _viewDetail.hidden = YES;
    _viewAttending.hidden = YES;
    _viewComment.hidden = NO;
    inputBar.hidden = NO;
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    [self getListComment];
}

-(void)getListComment{
    
    [arrayComment removeAllObjects];
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getListCommentWithEventID:_eventSelected.eventID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                [arrayComment addObject:dict];
            }
            
        }
        [_tableCommentView reloadData];
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];

}
#pragma mark Custom keyboard

-(void)customKeyboard{
    inputBar = [[YFInputBar alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY([UIScreen mainScreen].bounds)-108, 320, 44)];
    
    inputBar.backgroundColor = RGB(205, 55, 54);
    
    
    inputBar.delegate = self;
    inputBar.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    inputBar.textField.borderStyle = UITextBorderStyleRoundedRect;
    inputBar.resignFirstResponderWhenSend = YES;
    inputBar.textField.delegate = self;
    
    
    [self.view addSubview:inputBar];
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(void)showKeyBoard{
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
    
    
    
}
-(void)hiddenKeyBoard{
    [UIView animateWithDuration:0.3
                          delay:0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{

                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
    
    
}
-(void)addComment{
    [self.view endEditing:YES];
    if ([inputBar.textField.text isEqualToString:@""]) {
        return;
    }
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client addCommentWithEventID:_eventSelected.eventID fromUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] comment:inputBar.textField.text success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            inputBar.textField.text = @"";
            [self getListComment];
            
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];

}

@end
