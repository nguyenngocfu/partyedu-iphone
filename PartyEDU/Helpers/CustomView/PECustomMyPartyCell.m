//
//  PECustomMyPartyCell.m
//  PartyEDU
//
//  Created by thinhpham on 12/30/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PECustomMyPartyCell.h"

@implementation PECustomMyPartyCell

- (void)awakeFromNib
{
    // Initialization code
    _img_avarta.layer.cornerRadius = _img_avarta.frame.size.width/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
