//
//  DBHelper.m
//  PartyEDU
//
//  Created by thinhpham on 2/21/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "DBHelper.h"

@implementation DBHelper


@synthesize model=_model;

- (id)init{
    self = [super init];
    
    if (self) {
        int result = sqlite3_open([[Helpers getDbPath] UTF8String], &database);
        if (result != SQLITE_OK) {
            NSAssert1(0, @"Unable to open the sqlite database.'%s'", sqlite3_errmsg(database));
            return self;
        }
    }
    return self;
    
}



- (void)addNewMessage:(MessageModel*)m{
    const char *sql = "insert into message_history(friend_name,last_message,friend_photo,friend_id,user_id,last_message_time) Values(?, ?, ?, ?, ?, ?)";
    if(sqlite3_prepare_v2(database, sql, -1, &addStmt, NULL) != SQLITE_OK)
        NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
    
    
    // bind data to query
    sqlite3_bind_text(addStmt, 1, [m.friendName UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(addStmt, 2, [m.lastMessage UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(addStmt, 3, [m.friendPhoto UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(addStmt, 4, m.friendID);
    sqlite3_bind_int(addStmt, 5, m.userID);
    sqlite3_bind_text(addStmt, 6, [m.lastMessageTime UTF8String], -1, SQLITE_TRANSIENT);
    
    if(SQLITE_DONE != sqlite3_step(addStmt))
        NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
    else
        //Reset the add statement.
        sqlite3_reset(addStmt);
}

#pragma mark -delete data
- (void)deleteMessageWithFriendID:(int)friendID{
    // finalize old statement
    [self finalizeDelete];
    if(deleteStmt == nil) {
        NSString *query = [NSString stringWithFormat:@"DELETE FROM message_history where friend_id = %i", friendID];
        const char *sql = [query UTF8String];
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
    }
    
    if (SQLITE_DONE != sqlite3_step(deleteStmt))
        NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));
    
    sqlite3_reset(deleteStmt);
}

- (void)deleteAllMessage{
    // finalize old statement
    [self finalizeDelete];
    if(deleteStmt == nil) {
        const char *sql = "DELETE FROM message_history";
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
    }
    
    if (SQLITE_DONE != sqlite3_step(deleteStmt))
        NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));
    
    sqlite3_reset(deleteStmt);
}

#pragma mark -get list data
- (NSMutableArray*)getMessageFromUser: (int)userID
{
    NSMutableArray *retList = [[NSMutableArray alloc] init];
    [self finalizeSelect];
    NSString *query = [NSString stringWithFormat:@"select * FROM message_history where user_id = %i", userID];
    const char *sql = [query UTF8String];
    
    if(sqlite3_prepare_v2(database, sql, -1, &detailStmt, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(detailStmt) == SQLITE_ROW) {
            
            self.model = [[MessageModel alloc] init];
            self.model._id = sqlite3_column_int(detailStmt, 0);
            self.model.friendName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(detailStmt, 1)];
            self.model.lastMessage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(detailStmt, 2)];
            self.model.friendPhoto = [NSString stringWithUTF8String:(char *)sqlite3_column_text(detailStmt, 3)];
            self.model.friendID = sqlite3_column_int(detailStmt, 4);
            self.model.userID = sqlite3_column_int(detailStmt, 5);
            self.model.lastMessageTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(detailStmt, 6)];
            [retList addObject:self.model];
        }
        
    }
    
    else
        sqlite3_close(database);
    //NSLog(@"%d",[retList count]);
    return retList;
}


- (BOOL)hasDataRecordsWithFriendID: (int)friendID{
    NSInteger count = 0;
    // finalize old stmt
    [self finalizeSelect];
    
    NSString *query = [NSString stringWithFormat:@"SELECT COUNT(*) FROM message_history where friend_id = %i", friendID];
    const char *sql = [query UTF8String];
    
    if(sqlite3_prepare_v2(database, sql, -1, &detailStmt, NULL) == SQLITE_OK) {
        
        while(sqlite3_step(detailStmt) == SQLITE_ROW) {
            count = sqlite3_column_int(detailStmt, 0);
            break;
        }
    }
    
    else
        sqlite3_close(database);
    sqlite3_reset(detailStmt);
    //NSLog(@"Existing %i,  %d ", count, (count > 0));
    return (count > 0);
}






#pragma mark - finalize statement
- (void)closeAll{
    [self finalizeAdd];
    [self finalizeDelete];
    [self finalizeSelect];
    [self close];
}

// don't call this when call closeAll
- (void)close{
    if(database) sqlite3_close(database);
    database = nil;
}

- (void)finalizeAdd{
    if (addStmt) sqlite3_finalize(addStmt);
    addStmt = nil;
}

- (void)finalizeDelete{
    if (deleteStmt) sqlite3_finalize(deleteStmt);
    deleteStmt = nil;
}

- (void)finalizeSelect{
    if (detailStmt) sqlite3_finalize(detailStmt);
    detailStmt = nil;
}


@end
