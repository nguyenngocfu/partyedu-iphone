//
//  CategoryModel.h
//  PartyEDU
//
//  Created by Phạm Văn Thịnh on 1/5/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryModel : NSObject

@property (assign, nonatomic) int categoryID;
@property (strong, nonatomic) NSString *iconUrl;
@property (strong, nonatomic) NSString *categoryName;

- (id)initWithDictionary:(NSDictionary *)dict;
@end
