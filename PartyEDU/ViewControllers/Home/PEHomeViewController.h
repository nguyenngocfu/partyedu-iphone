//
//  PEHomeViewController.h
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"MCustomTextfield.h"

@interface PEHomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet MCustomTextfield *searchTextField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewHome;
- (IBAction)buttonViewMapClick:(id)sender;
- (IBAction)buttonSearchClick:(id)sender;


@end
