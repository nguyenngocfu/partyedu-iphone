//
//  PEPartyViewController.h
//  PartyEDU
//
//  Created by Phạm Văn Thịnh on 12/31/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "EventModel.h"
#import "DXPopover.h"

@interface PEPartyViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, GMSMapViewDelegate>

@property (assign, nonatomic) int categoryID;

@property (weak, nonatomic) IBOutlet UIView *viewCover;
@property (weak, nonatomic) IBOutlet UIView *detailEventView;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UITableView *tableListEvent;

@property (strong, nonatomic) NSMutableArray *arrayEvent;
@property (assign, nonatomic) BOOL isShowListEvent;

// custom popup
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) DXPopover *popover;


-(void)getCurrentLocation;
- (void)getListEvent;
- (void)initMapView;

- (void)searchEventWith:(NSString *)stringEvent;

@end
