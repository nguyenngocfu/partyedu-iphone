//
//  PEAppDelegate.h
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PESplashViewController.h"
#import "PELeftMenuViewController.h"
#import "PESideMenuContainerViewController.h"
#import "PEHomeViewController.h"
#import "LocationTracking.h"

@interface PEAppDelegate : UIResponder <UIApplicationDelegate, LocationDelegate>{
    NSTimer *timer;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) PESplashViewController *splashView;
@property (strong, nonatomic) PELeftMenuViewController *leftMenu;
@property (strong, nonatomic) PEHomeViewController *homeView;
@property (strong, nonatomic) PESideMenuContainerViewController *slideMenu;
@property (strong, nonatomic) UINavigationController *nav;
@property (strong, nonatomic) LocationTracking *locationTracking;
@property (strong, nonatomic) CLLocation *mylocation;

@property (assign, nonatomic) BOOL isCurrentMessageScreen;
- (void)initSlideMenu;
-(void)autoGetNewMessage;
@end
