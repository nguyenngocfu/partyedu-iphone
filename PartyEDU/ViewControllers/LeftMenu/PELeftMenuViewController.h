//
//  PELeftMenuViewController.h
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PECustomMenuCell.h"
@interface PELeftMenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UITableView *tableMenu;
@property (assign, nonatomic) int rowSelected;
- (IBAction)buttonSearchClick:(id)sender;

@end
