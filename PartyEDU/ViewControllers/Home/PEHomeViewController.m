//
//  PEHomeViewController.m
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PEHomeViewController.h"
#import "MFSideMenu.h"
#import "PEPartyViewController.h"
#import "Appclient.h"
#import "NSObject+SBJson.h"
#import "CategoryModel.h"
#import "UIButton+AFNetworking.h"
#import "UIImageView+AFNetworking.h"

@interface PEHomeViewController (){
    NSMutableArray *arrayCategory;
}

@end

@implementation PEHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayCategory = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
//    _scrollViewHome.contentSize = CGSizeMake(320, 504);
    _scrollViewHome.frame = CGRectMake(0, 64, 320, [[UIScreen mainScreen] bounds ].size.height-64-96);
    [self getListCategory];
    
}

#pragma mark Custom NavigationBar

- (UIBarButtonItem *)leftMenuBarButtonItem {
    // add menu button
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.frame = CGRectMake(0, 0, 25, 30);
    [menuButton setTitle:@"" forState:UIControlStateNormal];
    [menuButton setImage:[UIImage imageNamed:@"button_Menu.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:menuButton];
}
- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"icon_Location.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}

- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}
- (void)rightSideMenuButtonPressed:(id)sender {
    // Push to Myparties view
    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
    PEPartyViewController *parties = [[PEPartyViewController alloc]initWithNibName:@"PEPartyViewController" bundle:nil];
    [parties getCurrentLocation];
    [[slideMenu centerViewController] pushViewController:parties animated:NO];

}

#pragma mark getListCategory
- (void)getListCategory{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getListCategoryWithsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            
            // save customer_id
            NSArray *arrayCat = [results objectForKey:@"details"];
            for (NSDictionary *dict in arrayCat){
                CategoryModel *category = [[CategoryModel alloc] initWithDictionary:dict];
                if(![category.categoryName isEqualToString:@"Private"]) {
                    [arrayCategory addObject:category];
                }
                [self initScrollView];
            }
            
            
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}

-(void)initScrollView{
    int index = 0;
    for (CategoryModel *model in arrayCategory){
        UIButton * buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonCategory.frame = CGRectMake(32, 98+(20+56)*index, 256, 56);
        buttonCategory.tag = model.categoryID;
        buttonCategory.layer.cornerRadius = 6;
        [buttonCategory setImage:[UIImage imageNamed:@"button_Category.png"] forState:UIControlStateNormal];
        [buttonCategory addTarget:self action:@selector(buttonViewMapClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *iconImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 55, 55)];
        [iconImg setImageWithURL:[NSURL URLWithString:model.iconUrl] placeholderImage:nil];
        [iconImg setContentMode:UIViewContentModeScaleAspectFit];
        iconImg.backgroundColor = [UIColor clearColor];
        [buttonCategory addSubview:iconImg];
        
        UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 8, 160, 40)];
        textLabel.text = model.categoryName;
        textLabel.textAlignment = NSTextAlignmentLeft;
        textLabel.textColor = [UIColor whiteColor];
        textLabel.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:28];
        textLabel.backgroundColor = [UIColor clearColor];
        [buttonCategory addSubview:textLabel];
        

        [_scrollViewHome addSubview:buttonCategory];
        index ++;
    }
    [_scrollViewHome setContentSize:CGSizeMake(320, (98+(20+56)*arrayCategory.count+20))];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonViewMapClick:(id)sender {
    PEPartyViewController *partyView = [[PEPartyViewController alloc] initWithNibName:@"PEPartyViewController" bundle:nil];
    int catID = (int)[sender tag];
    partyView.categoryID = catID;
    [partyView getListEvent];
    [self.navigationController pushViewController:partyView animated:YES];
}

- (IBAction)buttonSearchClick:(id)sender {
    
    PEPartyViewController *partyView = [[PEPartyViewController alloc] initWithNibName:@"PEPartyViewController" bundle:nil];
    partyView.isShowListEvent = YES;
    [partyView searchEventWith:_searchTextField.text];
    
    [self.navigationController pushViewController:partyView animated:YES];
    
}
@end
