//
//  PEMyProfileViewController.m
//  PartyEDU
//
//  Created by thinhpham on 12/28/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PEMyProfileViewController.h"
#import "MFSideMenu.h"
#import "PEPartyViewController.h"
#import "UserModel.h"
#import "UIImageView+AFNetworking.h"
#import "PEEditProfileViewController.h"
@interface PEMyProfileViewController ()

@end

@implementation PEMyProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    _scrollViewProfile.contentSize = CGSizeMake(320, 504);
    _scrollViewProfile.frame = CGRectMake(0, 64, 320, [[UIScreen mainScreen] bounds ].size.height-64);
    
}

- (void)viewWillAppear:(BOOL)animated{
    _userInfo = [[UserModel alloc] initWithDictionary:[[Helpers shared] getUserInfo]];
    [self setUpUserInfo];
}

-(void)setUpUserInfo{
    _descriptionLabel.text = _userInfo.Description;
    _shortDescriptionLabel.text = _userInfo.sortDescription;
    _nameLabel.text = [NSString stringWithFormat:@"%@ %@",_userInfo.firstName, _userInfo.lastName ];
    _collegeLabel.text = _userInfo.college;
    [_userImageProfile setImageWithURL:[NSURL URLWithString:_userInfo.avataLink] placeholderImage:[UIImage imageNamed:@"avatar.png"]];
    _userImageProfile.layer.cornerRadius = _userImageProfile.frame.size.width/2;
//    _userImageProfile.layer.borderWidth = 1;
//    _userImageProfile.layer.borderColor = [UIColor blackColor].CGColor;
}

#pragma mark Custom NavigationBar

- (UIBarButtonItem *)leftMenuBarButtonItem {
    // add menu button
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.frame = CGRectMake(0, 0, 25, 30);
    [menuButton setTitle:@"" forState:UIControlStateNormal];
    [menuButton setImage:[UIImage imageNamed:@"button_Menu.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:menuButton];
}
- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"icon_Edit.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}
- (void)rightSideMenuButtonPressed:(id)sender {
//    // Push to Myparties view
//    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
//    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
//    PEPartyViewController *parties = [[PEPartyViewController alloc]initWithNibName:@"PEPartyViewController" bundle:nil];
//    [parties getCurrentLocation];
//    [[slideMenu centerViewController] pushViewController:parties animated:NO];
    
    PEEditProfileViewController *editProfileView = [[PEEditProfileViewController alloc]initWithNibName:@"PEEditProfileViewController" bundle:nil];
    editProfileView.userModel = _userInfo;
    [self.navigationController pushViewController:editProfileView animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
