//
//  CategoryModel.m
//  PartyEDU
//
//  Created by Phạm Văn Thịnh on 1/5/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "CategoryModel.h"

@implementation CategoryModel

- (id)initWithDictionary:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        _categoryID = [[dict objectForKey:@"id"] intValue];
        _categoryName = [dict objectForKey:@"Category"];
        _iconUrl = [dict objectForKey:@"icon"];
    }
    return self;
}
@end
