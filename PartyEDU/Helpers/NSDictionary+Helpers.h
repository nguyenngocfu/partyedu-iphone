

#import <Foundation/Foundation.h>

@interface NSDictionary (Helpers)
-(id)customObjectForKey:(NSString*)key;

-(NSInteger)intForKey:(NSString*)key;

-(NSString*)stringForKey:(NSString*)key;
@end
