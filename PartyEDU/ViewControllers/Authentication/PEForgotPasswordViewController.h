//
//  PEForgotPasswordViewController.h
//  PartyEDU
//
//  Created by LinhLV on 3/11/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PELoginViewController.h"
#import "MCustomTextfield.h"

@interface PEForgotPasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet MCustomTextfield *emailTextField;

- (IBAction)buttonResetClick:(id)sender;
@end

