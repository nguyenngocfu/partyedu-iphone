//
//  PEInviteViewController.h
//  PartyEDU
//
//  Created by Pham Van Thinh on 2/27/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventModel.h"
@protocol PopupInviteDelegate;

@interface PEInviteViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableEventInvites;
@property (assign, nonatomic) id <PopupInviteDelegate>delegate;
- (IBAction)cancelButtonClick:(id)sender;

@end

@protocol PopupInviteDelegate<NSObject>
@optional
- (void)dismissPopup;
- (void)inviteParty:(EventModel *)event;
@end