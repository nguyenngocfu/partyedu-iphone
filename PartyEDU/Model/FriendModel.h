//
//  FriendModel.h
//  PartyEDU
//
//  Created by Pham Van Thinh on 2/16/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendModel : NSObject
@property (assign, nonatomic) int friendID;
@property (strong, nonatomic) NSString *friendName;
@property (strong, nonatomic) NSString *friendEmail;
@property (strong, nonatomic) NSString *friendPhoto;
@property (strong, nonatomic) NSString *removeUrl;
@property (strong, nonatomic) NSString *blockUrl;
@property (strong, nonatomic) NSString *unblockUrl;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *block;
@property (strong, nonatomic) NSString *favoriteDrink;
@property (strong, nonatomic) NSString *college;

- (id)initWithDictionary:(NSDictionary *)dict;
@end
