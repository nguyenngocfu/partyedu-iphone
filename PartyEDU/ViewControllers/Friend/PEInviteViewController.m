//
//  PEInviteViewController.m
//  PartyEDU
//
//  Created by Pham Van Thinh on 2/27/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PEInviteViewController.h"
#import "Appclient.h"
#import "NSObject+SBJson.h"
#import "EventModel.h"
#import "AFNetworking.h"
@interface PEInviteViewController (){
    NSMutableArray *arrayEvent;
}

@end

@implementation PEInviteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayEvent = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self getPartyInvite];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getPartyInvite{
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getMyPartieWithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                EventModel *event = [[EventModel alloc]initWithDictionary:dict];
                [arrayEvent addObject:event];
            }
            [_tableEventInvites reloadData];
            
        }else if ([[results objectForKey:@"Status"] isEqualToString:@"010"]) {
            [[Helpers shared] alertStatus:@"You have not created any parties"title:@"" delegate:nil];
        }
        //        else {
        //            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        //        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}

#pragma -mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayEvent.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 44;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifier = @"PECustomMyPartyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
    }
    cell.textLabel.text = [[arrayEvent objectAtIndex:indexPath.row] eventName];
    cell.selectionStyle = UITableViewCellAccessoryCheckmark;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [self.delegate respondsToSelector:@selector(inviteParty:)]) {
        [self.delegate inviteParty:[arrayEvent objectAtIndex:indexPath.row]];
    }
}



- (IBAction)cancelButtonClick:(id)sender {
    if ( [self.delegate respondsToSelector:@selector(dismissPopup)]) {
        [self.delegate dismissPopup];
    }

}
@end
