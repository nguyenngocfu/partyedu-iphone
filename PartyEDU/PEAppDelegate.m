//
//  PEAppDelegate.m
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PEAppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Appclient.h"
#import "NSObject+SBJson.h"
#import "NSObject+SBJson.h"
#import "Appclient.h"
#import "AFNetworking.h"
#import "MessageModel.h"
#import "DBHelper.h"


static NSString *const kAPIKey = @"AIzaSyD3lbQ2Ckh4lS49G4svX8J4Qai42x1Sxb8";

@implementation PEAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    [GMSServices provideAPIKey:kAPIKey];
    [Helpers copyDbToPath];
    
    _splashView = [[PESplashViewController alloc] initWithNibName:@"PESplashViewController" bundle:nil];
    _nav = [[UINavigationController alloc]initWithRootViewController:_splashView];
    _nav.navigationController.navigationBarHidden = YES;
    _nav.navigationBar.tintColor = [UIColor whiteColor];

    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg_navigationBar.png"]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    
#if __IPHONE_8_0
    //Using Xcode6
    if ([[UIApplication sharedApplication]respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        //For iOS 8 and above
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:
         UIUserNotificationTypeAlert |
         UIUserNotificationTypeBadge |
         UIUserNotificationTypeSound categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    } else {
        // For iOS 7 and below
        [application registerForRemoteNotificationTypes:
         UIRemoteNotificationTypeBadge |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeSound];
    }
#else
    // For Xcode5
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
#endif
    
//    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
//    
//    NSArray *fontNames;
//    NSInteger indFamily, indFont;
//    for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
//    {
//        NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
//        fontNames = [[NSArray alloc] initWithArray:
//                     [UIFont fontNamesForFamilyName:
//                      [familyNames objectAtIndex:indFamily]]];
//        for (indFont=0; indFont<[fontNames count]; ++indFont)
//        {
//            NSLog(@"    Font name: %@", [fontNames objectAtIndex:indFont]);
//        }
//    }
    
    // Handle launching from a notification
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        // Set icon badge number to zero
        application.applicationIconBadgeNumber = 0;
    }
    
    
    [self getListCategory];
    [self getCurrentLocation];

    
    self.window.rootViewController = _nav;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)initSlideMenu{
    _leftMenu = [[PELeftMenuViewController alloc] initWithNibName:@"PELeftMenuViewController" bundle:nil];
    _homeView = [[PEHomeViewController alloc] initWithNibName:@"PEHomeViewController" bundle:nil];
    UINavigationController *navRight = [[UINavigationController alloc]initWithRootViewController:_homeView];
    navRight.navigationBar.tintColor = [UIColor whiteColor];
    
    navRight.navigationController.navigationBarHidden = NO;
    
    // init slideMenu
    _slideMenu = [[PESideMenuContainerViewController alloc]init];
    _slideMenu.leftMenuViewController = _leftMenu;
    _slideMenu.leftMenuWidth = WIDTH_LEFT_SLIDE;
    _slideMenu.centerViewController = navRight;
    _slideMenu.panMode = MFSideMenuPanModeDefault;
}

#pragma mark Location Delegate

-(void)getCurrentLocation{
    // init locationTracking
    _locationTracking = [[LocationTracking alloc] init];
    _locationTracking.delegateLocation = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if (DETECT_OS8 && [_locationTracking respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationTracking requestWhenInUseAuthorization];
    }
    //    _locationTracking.checkGetPostCode = YES;
    [_locationTracking updateLocation];
}


-(void)getLocation:(CLLocation *)location{
    //Get List of shop near me
    _mylocation = location;
    
}

-(void)failedGetLocation{
    
}
#pragma mark getListCategory
- (void)getListCategory{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getListCategoryWithsuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            
            // save customer_id
            NSArray *arrayCat = [results objectForKey:@"details"];
            NSMutableDictionary *dictCategory = [[NSMutableDictionary alloc]init];
            for (NSDictionary *dict in arrayCat){
                [dictCategory setValue:[dict objectForKey:@"icon"] forKey:[dict objectForKey:@"Category"]];
            }
            [[Helpers shared] saveCategoryList:dictCategory];
            
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
    }];
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:notification.alertAction
                                                        message:notification.alertBody
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self saveMessageNotification:notification.userInfo];
        
    }
    
//    // Request to reload table view data
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
//    
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
}
-(void)saveMessageNotification:(NSDictionary *)dict{
    MessageModel *message = [[MessageModel alloc]initWithDictionary:dict];
    
    message.userID = (int)[[Helpers shared] getIntegerForKey:USER_ID];
    message.friendID = [[dict objectForKey:@"friend_id"] intValue];
    
    DBHelper *db = [[DBHelper alloc]init];
    if ([db hasDataRecordsWithFriendID:message.friendID]) {
        [db deleteMessageWithFriendID:message.friendID];
    }
    [db addNewMessage:message];
    
    
}

#pragma mark getNewNotification
#pragma mark getListMessage

-(void)autoGetNewMessage{
    if (!timer ) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                 target: self
                                               selector:@selector(getNewNotification)
                                               userInfo: nil repeats:YES];
    }

}

-(void)getNewNotification{
    
    if (![[Helpers shared]checkNetworkAvailable] || (int)[[Helpers shared] getIntegerForKey:USER_ID]==0 || _isCurrentMessageScreen) {
        
        return;
    }
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getNewMessageWithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID]success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            
            if (eventArr.count > 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NTF_NEW_MESSAGE" object:nil];
                MessageModel *message = [[MessageModel alloc]initWithDictionary:[eventArr firstObject]];
                
                // local push text
                
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [NSDate date];
                localNotification.alertBody = message.lastMessage;
                localNotification.alertAction = message.friendName;
                localNotification.userInfo = [eventArr firstObject];
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);

    }];
    
}


@end
