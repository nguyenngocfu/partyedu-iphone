//
//  PERegisterViewController.m
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PERegisterViewController.h"
#import "Appclient.h"
#import "NSObject+SBJson.h"

@interface PERegisterViewController ()

@end

@implementation PERegisterViewController

@synthesize termOfUseButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden = NO;
    [self.navigationItem setHidesBackButton:NO];
    _scrollViewRegister.contentSize = CGSizeMake(320, 568);
    _scrollViewRegister.frame = CGRectMake(0, 64, 320, [[UIScreen mainScreen] bounds ].size.height-64);
    
}
-(void)viewWillAppear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonAcceptClick:(id)sender {
    if ([_buttonAccept isSelected]) {
        _buttonAccept.selected = NO;
    } else {
        _buttonAccept.selected = YES;
    }
}

- (IBAction)buttonTermOfUseClick:(id)sender {
    // Open term of use page
    NSString *urlString = @"http://www.partyedu.com/terms-and-conditions";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[urlString
                                                                     stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
}

- (IBAction)buttonRegisterClick:(id)sender {
    [self.view endEditing:YES];
    if ([_nameTextField.text isEqualToString:@""] || [_emailTextField.text isEqualToString:@""] || [_passTextField.text isEqualToString:@""] || [_rePassTextField.text isEqualToString:@""] ) {
        [[Helpers shared] alertStatus:@"Please fill all field!" title:@"" delegate:nil];
        return;
    }

    if (![[Helpers shared]validateEmailWithString:_emailTextField.text]) {
        [[Helpers shared] alertStatus:@"Invalid email address!" title:@"" delegate:nil];
        return;
    }
    if (![_passTextField.text isEqualToString:_rePassTextField.text ]) {
        [[Helpers shared] alertStatus:@"Passwords don't match!" title:@"" delegate:nil];
        return;
    }
    if (![_buttonAccept isSelected]) {
        [[Helpers shared] alertStatus:@"Please accept all condition!" title:@"" delegate:nil];
        return;

    }
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client registerWithName:_nameTextField.text email:_emailTextField.text pass:_passTextField.text success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            [[Helpers shared] alertPopupStatus:@"Your account has been registed successfully!" title:@"" delegate:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else {
            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];

}
@end
