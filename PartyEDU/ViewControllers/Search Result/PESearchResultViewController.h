//
//  PESearchResultViewController.h
//  PartyEDU
//
//  Created by thinhpham on 2/17/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PESearchResultViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableFriend;
-(void)getListSearchResult:(NSString *)keyWord;
@end
