//
//  PEMessageViewController.h
//  PartyEDU
//
//  Created by Pham Van Thinh on 2/16/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PEMessageViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableHistoryMessage;
@end
