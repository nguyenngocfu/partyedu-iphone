//
//  PELoginViewController.m
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PEForgotPasswordViewController.h"
#import "Appclient.h"
#import "NSObject+SBJson.h"
#import "UserModel.h"

@interface PEForgotPasswordViewController ()

@end

@implementation PEForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //check user has been login
    if ((int)[[Helpers shared] getIntegerForKey:USER_ID] >0) {
        _emailTextField.text = [[Helpers shared] getObjectForKey:USER_EMAIL];

    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
    [self.navigationItem setHidesBackButton:YES];
}

-(void)pushToLoginViewAnimation:(BOOL)animated{
    PELoginViewController *loginView = [[PELoginViewController alloc]initWithNibName:@"PELoginViewController" bundle:nil];
    [self.navigationController pushViewController:loginView animated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonResetClick:(id)sender {
    [self.view endEditing:YES];
    if ([_emailTextField.text isEqualToString:@""]) {
        [[Helpers shared] alertStatus:@"Please fill all field!" title:@"" delegate:nil];
        return;
    }
    
    if (![[Helpers shared]validateEmailWithString:_emailTextField.text]) {
        [[Helpers shared] alertStatus:@"Invalid email address!" title:@"" delegate:nil];
        return;
    }
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client forgotPassword:_emailTextField.text success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            
            // Show alert
            [[Helpers shared] alertStatus:@"Your password has been reset. Please check your email." title:@"" delegate:nil];
            [self pushToLoginViewAnimation:YES];
            
        } else if ([[results objectForKey:@"Status"] isEqualToString:@"010"]) {
            [[Helpers shared] alertStatus:@"The information you entered is incorrect." title:@"" delegate:nil];
        } else {
            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
}

@end
