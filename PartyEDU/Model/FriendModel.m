//
//  FriendModel.m
//  PartyEDU
//
//  Created by Pham Van Thinh on 2/16/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "FriendModel.h"

@implementation FriendModel

- (id)initWithDictionary:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        _friendID = [[dict objectForKey:@"friend Id"] intValue];
        _friendName = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"firsname"],[dict objectForKey:@"lastname"]];
        _friendPhoto = [dict objectForKey:@"photoName"];
        _blockUrl = [dict objectForKey:@"block_url"];
        _removeUrl = [dict objectForKey:@"remove_url"];
        _unblockUrl = [dict objectForKey:@"unblock_url"];
        _email = [dict objectForKey:@"email"];
        _block = [dict objectForKey:@"Status"];
        _favoriteDrink = [dict objectForKey:@"sort_description"];
        _college = [dict objectForKey:@"college"];
    }
    return self;
}

@end
