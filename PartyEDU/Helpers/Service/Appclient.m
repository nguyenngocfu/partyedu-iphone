#import "Appclient.h"
#import "NSDictionary+Helpers.h"

@interface AppClient()
-(NSString*)getMethodName:(METHOD)m;

-(void)showLogOfSuccessfulRequest:(AFHTTPRequestOperation*)operation Response:(id)responseObject;

-(void)showLogOfFailedRequest:(AFHTTPRequestOperation*)operation Error:(NSError*)error;

@end

@implementation AppClient


#pragma mark - init

- (id)initJSONClient
{
    NSURL* url = [NSURL URLWithString:BASE_URL];
    
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
//    [self setDefaultHeader:@"Accept" value:@"application/json"];
    [AFJSONRequestOperation addAcceptableContentTypes:
     [NSSet setWithObject:@"text/plain"]];
    
    [self setParameterEncoding:AFFormURLParameterEncoding];
    
    _debug = NO;
    
    return self;
}

-(id)initWithToken:(NSString*)tk
{
    self = [self initJSONClient];
    
    _token = tk;
    
    return self;
}

#pragma mark - AFHTTPClient Services

-(NSString*)getMethodName:(METHOD)m
{
    switch (m) {
        case METHOD_POST:
            return @"POST";
        case METHOD_PUT:
            return @"PUT";
        case METHOD_DELETE:
            return @"DELETE";
        case METHOD_GET:
            return @"GET";
    }
    return @"GET";
}

-(void)cancel
{
    [[self operationQueue] cancelAllOperations];
}

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters
{
    NSMutableURLRequest *request = [super requestWithMethod:method path:path parameters:parameters];
    [request setTimeoutInterval:TIME_OUT];
    return request;
}

-(void)requestByMethod:(METHOD)method
             widthPath:(NSString*)path
        withParameters:(NSDictionary*)dict
               success:(SuccessResponse)success
               failure:(FailureResponse)failure
{
    if (_debug) {
        NSLog(@"Parameters %@",dict);
    }
    
    NSURLRequest *request = [self requestWithMethod:[self getMethodName:method] path:path parameters:dict];
    
    AFHTTPRequestOperation *operation =
    [self HTTPRequestOperationWithRequest:request
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      
                                      _statusCode = [[operation response] statusCode];
                                      
                                      if (_debug) {
                                          [self showLogOfSuccessfulRequest:operation Response:responseObject];
                                      }
                                      
                                      if (success) {
                                          success(operation,responseObject);
                                      }
                                      
                                  }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      
                                      [self showLogOfFailedRequest:operation Error:error];
                                      
                                      _statusCode = [[operation response] statusCode];
                                      
                                      if (error.code == NSURLErrorCancelled) {
                                          goto CALL_FAILURE;
                                      }
                                      
                                      if (error.code == NSURLErrorTimedOut) {
                                          [[Helpers shared] alertStatus:@"The request timed out, please try again later!" title:@"Error" delegate:nil];
                                          goto CALL_FAILURE;
                                      }
                                      
                                      if (error.code == NSURLErrorNotConnectedToInternet) {
                                          [[Helpers shared] alertStatus:@"Can't connect to server, please try again later!" title:@"Error" delegate:nil];
                                          goto CALL_FAILURE;
                                      }
                                      
                                      if (_statusCode == 401) {
                                          id response = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
                                          if ([response isKindOfClass:[NSDictionary class]]) {
                                              NSDictionary* dict = (NSDictionary*)response;
                                              if ([[dict stringForKey:@"error"] isEqualToString:@"Invalid authentication token."]) {
//                                                  [APP_DELEGATE presentModalLoginViewController];
                                                  goto CALL_FAILURE;
                                              }
                                              
                                          }
                                      }
                                      
//#ifdef APPDEBUG
//                                      [APP_DELEGATE alertView:@"Error" withMessage:@"Something was wrong. Please contact to Developer" andButton:@"OK"];
//#endif
                                      
                                  CALL_FAILURE:
                                      if (failure) {
                                          failure(operation,error);
                                      }
                                      
                                  }];
    
    [self enqueueHTTPRequestOperation:operation];
}

+(BOOL)success:(NSDictionary*)success
{
    NSString* s = [success stringForKey:@"success"];
    return [s isEqualToString:@"1"];
}

-(void)showLogOfSuccessfulRequest:(AFHTTPRequestOperation*)operation Response:(id)responseObject
{
    NSLog(@"************************************** successful request ******************************************");
    NSLog(@"url %@",[[operation response] URL]);
    NSLog(@"status code %ld",(long)[[operation response] statusCode]);
    NSLog(@"response %@",responseObject);
    NSLog(@"************************************** end ******************************************");
}

-(void)showLogOfFailedRequest:(AFHTTPRequestOperation*)operation Error:(NSError*)error
{
    NSLog(@"************************************** failed request ******************************************");
    NSLog(@"url %@",[[operation response] URL]);
    NSLog(@"status code %ld",(long)[[operation response] statusCode]);
    NSLog(@"response %@\n%@",[operation responseString],[error localizedDescription]);
    NSLog(@"************************************** end ******************************************");
}


#pragma mark - API Services

#pragma mark Autithencation

-(void) registerWithName:(NSString *)name email:(NSString *)email pass:(NSString *)pass  success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [[NSString stringWithFormat:@"/partyedu/api/signup.php?email=%@&password=%@&repassword=%@&name=%@", email, pass, pass, name] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void) loginWithEmail:(NSString *)email pass:(NSString *)pass  success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/signin.php?email=%@&password=%@", email, pass];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void) forgotPassword: (NSString*)email success:(SuccessResponse)success
               failure:(FailureResponse)failure{
    NSString *path = [NSString stringWithFormat:@"partyedu/api/forgotpassword.php?email=%@", email];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void)updateUserProfile:(UserModel *)model success:(SuccessResponse)success failure:(FailureResponse)failure{
    NSString *name = [NSString stringWithFormat:@"%@ %@",model.firstName, model.lastName];
    
    NSString *path = [[NSString stringWithFormat:@"partyedu/api/update_profile.php?email=%@&name=%@&customer_id=%i&sort_description=%@&description=%@&college=%@", model.email, name,model.userID, model.sortDescription, model.Description, model.college] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}


#pragma mark Category
-(void) getListCategoryWithsuccess:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = @"partyedu/api/category.php";
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
#pragma mark State
-(void) getListStateWithsuccess:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = @"partyedu/api/state.php";
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

#pragma mark Event
-(void) getListEventWithCategoryID:(int) categoryID WithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure{
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-search.php?category=%i", categoryID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void) getMyPartieWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events.php?user_id=%i", userID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) getMyAttendingWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-search.php?user=%i", userID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) createNewEvent:(EventModel *) event success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [[NSString stringWithFormat:@"partyedu/api/manage-event.php?mode=add&user_id=%i&category_id=%i&event_name=%@&event_date=%@&event_time=%@&address=%@&city=%@&state=%@&zip_code=%@&cover=%@&college_affi=%@&other_details=%@&password=abc&latitude=%@&longitude=%@", event.userID, event.categoryID, event.eventName, event.eventDate, event.eventTime, event.address, event.city, event.stateEvent, event.zip_code, event.cover,event.collage,event.otherDetail,event.latitue,event.longtitue]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) getListMemberWithEventID:(int) eventID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-member.php?event_id=%i", eventID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void)searchEventWithTime:(NSString *)time WithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure{
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-search.php?user_id=%d&search=@""&daterange=%@", userID, [time isEqualToString:POPOVER_CELL_0]? @"" : time];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void)searchEventWithString:(NSString *)stringEvent WithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure{
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-search.php?user_id=%d&search=%@&daterange=", userID, stringEvent];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void)searchEventWithLocation:(CLLocation *)location success:(SuccessResponse)success failure:(FailureResponse)failure{
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-search.php?latitude=%f&longitude=%f", location.coordinate.latitude, location.coordinate.longitude];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void)getListUserJoinWithEvent:(int) eventID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-member.php?event_id=%i", eventID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}


-(void) joinPartieWithUserID:(int) userID andEventID: (int) eventID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-join.php?event_id=%i&user_id=%i&mode=join", eventID,userID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) unJoinPartieWithUserID:(int) userID andEventID: (int) eventID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-join.php?event_id=%i&user_id=%i&mode=unjoin", eventID,userID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) getPartyInviteWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-invite.php?user_id=%i", userID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) sendInvitePartyWithEventID:(int) eventID fromFriendID: (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/events-invite.php?event_id=%i&friend_id=%i&mode=add", eventID, friendID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

#pragma mark Friend

-(void) getFriendWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/user_friends.php?user_id=%i", userID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) getListPendingRequestWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/friends_request.php?user_id=%i", userID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) searchFriendWithUserID:(int) userID andKeyWord : (NSString *) keyWord success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [[NSString stringWithFormat:@"partyedu/api/search_friends.php?keyword=%@&user_id=%i", keyWord, userID]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) getDetailUser:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/friends_details.php?user_id=%i&friend_id=%i", userID, friendID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) addFriend:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/friends_join.php?user_id=%i&friend_id=%i&mode=add", userID, friendID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void) blockFriend:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/friends_join.php?user_id=%i&friend_id=%i&mode=block", userID, friendID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void) unBlockFriend:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/friends_join.php?user_id=%i&friend_id=%i&mode=unblock", userID, friendID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void) removeFriend:(NSString *) removeURL success:(SuccessResponse)success failure:(FailureResponse)failure{
    [self requestByMethod:METHOD_GET widthPath:removeURL withParameters:nil success:success failure:failure];
}
-(void) acceptRequest:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/friends_join.php?user_id=%i&friend_id=%i&mode=accept", userID, friendID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void) denyRequest:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/friends_join.php?user_id=%i&friend_id=%i&mode=reject", userID, friendID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

#pragma mark comment
-(void) getListCommentWithEventID:(int) eventID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/comments.php?event_id=%i", eventID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void) addCommentWithEventID:(int) eventID fromUserID: (int) userID comment: (NSString *) comment success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [[NSString stringWithFormat:@"partyedu/api/manage-comment.php?event_id=%i&user_id=%i&comments=%@", eventID, userID, comment]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

#pragma mark message

-(void) getListMessageHistory:(int) userID andFriendID : (int) friendID  success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/user-message.php?friend_id=%i&user_id=%i", userID, friendID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
-(void) sendMessageWithUserID:(int) userID andFriendID: (int) friendID message: (NSString *) message success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [[NSString stringWithFormat:@"partyedu/api/manage-message.php?friend_id=%i&user_id=%i&messege=%@", friendID, userID, message]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

-(void) getNewMessageWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure{
    
    NSString *path = [NSString stringWithFormat:@"partyedu/api/user-message-notification.php?user_id=%i", userID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
- (void)getAllMessageWithUserId:(int)userID success:(SuccessResponse)success failure:(FailureResponse)failure{
    NSString *path = [NSString stringWithFormat:@"partyedu/api/user-message-all.php?user_id=%d", userID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}
#pragma mark - FRIEND
- (void)getFrienDetail:(int)userID andFriendID:(int)friendID success:(SuccessResponse)success failure:(FailureResponse)failure{
    NSString *path = [NSString stringWithFormat:@"partyedu/api/friends_details.php?friend_id=%i&user_id=%i", friendID, userID];
    [self requestByMethod:METHOD_GET widthPath:path withParameters:nil success:success failure:failure];
}

@end




