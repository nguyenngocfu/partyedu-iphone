//
//  PECustomChatFriendCell.m
//  PartyEDU
//
//  Created by thinhpham on 2/22/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PECustomChatFriendCell.h"

@implementation PECustomChatFriendCell

- (void)awakeFromNib {
    // Initialization code
    _avartaImage.layer.cornerRadius = _avartaImage.frame.size.width/2;
    _textMessage.layer.cornerRadius = 6;
    _textMessage.layer.borderWidth = 1;
    _textMessage.layer.borderColor = [UIColor blackColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
