#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "EventModel.h"

typedef enum {
    METHOD_GET = 0,
    METHOD_POST,
    METHOD_PUT,
    METHOD_DELETE,
} METHOD;

typedef enum{
    AppClientTypeNoDefine = 0,
    AppClientTypeLogin,
}AppClientType;

typedef void (^SuccessResponse)(AFHTTPRequestOperation *operation, id responseObject);
typedef void (^FailureResponse)(AFHTTPRequestOperation *operation, NSError *error);

@interface AppClient : AFHTTPClient

@property(nonatomic,assign) BOOL            debug;
@property(nonatomic,strong) NSString*       token;
@property(nonatomic,assign) NSInteger       statusCode;

- (id)initJSONClient;

-(id)initWithToken:(NSString*)tk;

-(void)cancel;

-(void)requestByMethod:(METHOD)method widthPath:(NSString*)path withParameters:(NSDictionary*)dict success:(SuccessResponse)success failure:(FailureResponse)failure;

// Autithencation
-(void) registerWithName:(NSString *)name email:(NSString *)email pass:(NSString *)pass  success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) loginWithEmail:(NSString *)email pass:(NSString *)pass  success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) forgotPassword: (NSString*)email success:(SuccessResponse)success
    failure:(FailureResponse)failure;
-(void)updateUserProfile:(UserModel *)model success:(SuccessResponse)success failure:(FailureResponse)failure;

// Category
-(void) getListCategoryWithsuccess:(SuccessResponse)success failure:(FailureResponse)failure;

// State
-(void) getListStateWithsuccess:(SuccessResponse)success failure:(FailureResponse)failure;

// Event
-(void) getListEventWithCategoryID:(int) categoryID WithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) getMyPartieWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) getMyAttendingWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) createNewEvent:(EventModel *) event success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) getListMemberWithEventID:(int) eventID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void)searchEventWithTime:(NSString *)time WithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void)searchEventWithString:(NSString *)stringEvent WithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void)searchEventWithLocation:(CLLocation *)location success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void)getListUserJoinWithEvent:(int) eventID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) joinPartieWithUserID:(int) userID andEventID: (int) eventID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) unJoinPartieWithUserID:(int) userID andEventID: (int) eventID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) getPartyInviteWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) sendInvitePartyWithEventID:(int) eventID fromFriendID: (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure;

//Friend
-(void) getFriendWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) getListPendingRequestWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) searchFriendWithUserID:(int) userID andKeyWord : (NSString *) keyWord success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) getDetailUser:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) addFriend:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) blockFriend:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) unBlockFriend:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) removeFriend:(NSString *) removeURL success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) acceptRequest:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) denyRequest:(int) userID andFriendID : (int) friendID success:(SuccessResponse)success failure:(FailureResponse)failure;

//Comment
-(void) getListCommentWithEventID:(int) eventID success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) addCommentWithEventID:(int) eventID fromUserID: (int) userID comment: (NSString *) comment success:(SuccessResponse)success failure:(FailureResponse)failure;
//Message
-(void) getListMessageHistory:(int) userID andFriendID : (int) friendID  success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) sendMessageWithUserID:(int) userID andFriendID: (int) friendID message: (NSString *) message success:(SuccessResponse)success failure:(FailureResponse)failure;
-(void) getNewMessageWithUserID:(int) userID success:(SuccessResponse)success failure:(FailureResponse)failure;
- (void)getAllMessageWithUserId:(int)userID success:(SuccessResponse)success failure:(FailureResponse)failure;
//Friend
- (void)getFrienDetail:(int)userID andFriendID:(int)friendID success:(SuccessResponse)success failure:(FailureResponse)failure;
@end