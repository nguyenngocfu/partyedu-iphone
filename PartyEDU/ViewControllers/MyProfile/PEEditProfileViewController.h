//
//  PEEditProfileViewController.h
//  PartyEDU
//
//  Created by Pham Van Thinh on 1/21/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"
#import "CustomTextView.h"

@interface PEEditProfileViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewProfile;
@property (weak, nonatomic) IBOutlet UIView *viewInfoUser;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *collegeTextField;
@property (weak, nonatomic) IBOutlet UITextField *shortDescriptionTextField;
@property (weak, nonatomic) IBOutlet CustomTextView *descriptionTextView;

@property (strong, nonatomic) UserModel *userModel;

- (IBAction)buttonChooseImageClick:(id)sender;
- (IBAction)buttonSaveClick:(id)sender;

@end
