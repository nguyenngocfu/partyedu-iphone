//
//  PECustomMessageCell.h
//  PartyEDU
//
//  Created by thinhpham on 2/20/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PECustomMessageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avartaImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *textMessage;
@property (weak, nonatomic) IBOutlet UILabel *dateTime;

@end
