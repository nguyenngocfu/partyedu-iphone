//
//  PEPendingRequestViewController.m
//  PartyEDU
//
//  Created by Pham Van Thinh on 2/16/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PEPendingRequestViewController.h"
#import "AFNetworking.h"
#import "NSObject+SBJson.h"
#import "AFNetworking.h"
#import "Appclient.h"
#import "MFSideMenu.h"
#import "PEPartyViewController.h"
#import "FriendModel.h"

@interface PEPendingRequestViewController (){
    NSMutableArray *arrayFriend;
}

@end

@implementation PEPendingRequestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayFriend = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [self getListFriend];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getListFriend{
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    [arrayFriend removeAllObjects];
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getListPendingRequestWithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                [arrayFriend addObject:dict];
            }
            
        } else if ([[results objectForKey:@"Status"] isEqualToString:@"010"]) {
            [[Helpers shared] alertStatus:@"You have no pending requests"title:@"" delegate:nil];
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
        [_tableFriend reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        MBProgressHiding();
    }];
    
}


#pragma mark Custom NavigationBar

- (UIBarButtonItem *)leftMenuBarButtonItem {
    // add menu button
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.frame = CGRectMake(0, 0, 25, 30);
    [menuButton setTitle:@"" forState:UIControlStateNormal];
    [menuButton setImage:[UIImage imageNamed:@"button_Menu.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:menuButton];
}
- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"icon_Location.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}
- (void)rightSideMenuButtonPressed:(id)sender {
    // Push to Myparties view
    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
    PEPartyViewController *parties = [[PEPartyViewController alloc]initWithNibName:@"PEPartyViewController" bundle:nil];
    [parties getCurrentLocation];
    [[slideMenu centerViewController] pushViewController:parties animated:NO];
}

#pragma -mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayFriend.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifier = @"PECustomFriendCell";
    PECustomFriendCell *cell = (PECustomFriendCell*)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil) {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECustomFriendCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.titleUserName.text = [[arrayFriend objectAtIndex:indexPath.row]objectForKey:@"name"];
    [cell.img_avarta setImageWithURL:[NSURL URLWithString:[[arrayFriend objectAtIndex:indexPath.row]objectForKey:@"photoName"]] placeholderImage:[UIImage imageNamed:@"avatar.png"]];
    cell.favoriteDrinkLabel.text = [[arrayFriend objectAtIndex:indexPath.row] objectForKey:@"sort_description"];
    cell.collegeLabel.text = [[arrayFriend objectAtIndex:indexPath.row] objectForKey:@"college"];
    cell.buttonInvite.hidden = YES;
    cell.buttonMessage.hidden = YES;
    cell.buttonDeny.hidden = NO;
    cell.tag = indexPath.row;
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark customFriendDelegate

-(void) acceptFriend:(NSInteger) tag{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client acceptRequest:(int)[[Helpers shared] getIntegerForKey:USER_ID] andFriendID:[[[arrayFriend objectAtIndex:tag] objectForKey:@"friend_id"] intValue] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            [self getListFriend];
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        MBProgressHiding();
    }];

}
#pragma mark customFriendDelegate

-(void) denyFriend:(NSInteger) tag{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client acceptRequest:(int)[[Helpers shared] getIntegerForKey:USER_ID] andFriendID:[[[arrayFriend objectAtIndex:tag] objectForKey:@"friend_id"] intValue] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            [self getListFriend];
        }
        //        else {
        //            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        //        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        MBProgressHiding();
    }];
    
}


@end
