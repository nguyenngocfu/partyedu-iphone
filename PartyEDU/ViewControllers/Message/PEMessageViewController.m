//
//  PEMessageViewController.m
//  PartyEDU
//
//  Created by Pham Van Thinh on 2/16/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PEMessageViewController.h"
#import "MFSideMenu.h"
#import "FriendModel.h"
#import "DBHelper.h"
#import "PEPartyViewController.h"
#import "PECustomMessageCell.h"
#import "AFNetworking.h"
#import "PEDetailViewController.h"
#import "Appclient.h"
#import "NSObject+SBJson.h"
@interface PEMessageViewController (){
    NSMutableArray *arrayHistoryMessage;
    int _getFrienAvatarCount;
}

@end

@implementation PEMessageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayHistoryMessage = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    _getFrienAvatarCount = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMessage) name:@"NTF_NEW_MESSAGE" object:nil];
}
- (void)newMessage{
    [self getAllMessage:(int)[[Helpers shared] getIntegerForKey:USER_ID]];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    DBHelper *helper = [[DBHelper alloc]init];
//    arrayHistoryMessage = [helper getMessageFromUser:(int)[[Helpers shared] getIntegerForKey:USER_ID]];
//    for (MessageModel *message in arrayHistoryMessage) {
//        [self getFriendPhoto:message];
//    }
//    [_tableHistoryMessage reloadData];
    [self getAllMessage:(int)[[Helpers shared] getIntegerForKey:USER_ID]];
}
- (void)getAllMessage:(int)userID{
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getAllMessageWithUserId:userID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            //            for (NSDictionary *dict in eventArr) {
            //                MessageModel *message = [[MessageModel alloc]initWithDictionary:dict];
            //                [arrayMessage addObject:message];
            //            }
            [arrayHistoryMessage removeAllObjects];
            for (int i = (int)eventArr.count-1; i >=0; i--) {
                MessageModel *message = [[MessageModel alloc]initWithDictionary:[eventArr objectAtIndex:i]];
                [arrayHistoryMessage addObject:message];
            }
            [self.tableHistoryMessage reloadData];
//            if (_tableMessageDetail.contentSize.height > _tableMessageDetail.frame.size.height)
//            {
//                CGPoint offset = CGPointMake(0, _tableMessageDetail.contentSize.height -     _tableMessageDetail.frame.size.height);
//                [_tableMessageDetail setContentOffset:offset animated:NO];
//            }
            
        }
        //        else {
        //            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
}
#pragma mark Custom NavigationBar

- (UIBarButtonItem *)leftMenuBarButtonItem {
    // add menu button
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.frame = CGRectMake(0, 0, 25, 30);
    [menuButton setTitle:@"" forState:UIControlStateNormal];
    [menuButton setImage:[UIImage imageNamed:@"button_Menu.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:menuButton];
}
- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}


- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"icon_Location.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}
- (void)rightSideMenuButtonPressed:(id)sender {
    
    // Push to Myparties view
    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
    PEPartyViewController *parties = [[PEPartyViewController alloc]initWithNibName:@"PEPartyViewController" bundle:nil];
    [parties getCurrentLocation];
    [[slideMenu centerViewController] pushViewController:parties animated:NO];
}
#pragma -mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayHistoryMessage.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifier = @"PECustomMessageCell";
    PECustomMessageCell *cell = (PECustomMessageCell*)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil) {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECustomMessageCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.userName.text = [[arrayHistoryMessage objectAtIndex:indexPath.row] friendName];
    cell.textMessage.text = [[arrayHistoryMessage objectAtIndex:indexPath.row] lastMessage];
    cell.dateTime.text = [[arrayHistoryMessage objectAtIndex:indexPath.row] lastMessageTime];
    
    [cell.avartaImage setImageWithURL:[NSURL URLWithString:[[arrayHistoryMessage objectAtIndex:indexPath.row] friendPhoto]] placeholderImage:[UIImage imageNamed:@"no_image"]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PEDetailViewController *detailMessageView = [[PEDetailViewController alloc]initWithNibName:@"PEDetailViewController" bundle:nil];
    MessageModel *messageModel = [arrayHistoryMessage objectAtIndex:indexPath.row];
    detailMessageView.friendInfo = [[FriendModel alloc]init];
    detailMessageView.friendInfo.friendName = messageModel.friendName;
    detailMessageView.friendInfo.friendID = messageModel.friendID;
    
    [self.navigationController pushViewController:detailMessageView animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
