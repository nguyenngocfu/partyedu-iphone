//
//  PECommentCell.m
//  PartyEDU
//
//  Created by thinhpham on 2/18/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PECommentCell.h"

@implementation PECommentCell

- (void)awakeFromNib
{
    // Initialization code
    _avartaUser.layer.cornerRadius = _avartaUser.frame.size.width/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
