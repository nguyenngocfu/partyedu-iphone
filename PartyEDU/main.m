//
//  main.m
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PEAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PEAppDelegate class]));
    }
}
