//
//  MessageModel.m
//  PartyEDU
//
//  Created by thinhpham on 2/21/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "MessageModel.h"

@implementation MessageModel

- (id)initWithDictionary:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        _friendName = [dict objectForKey:@"user_name"];
        _friendPhoto = [dict objectForKey:@"photoName"];
        _lastMessage = [dict objectForKey:@"messege"];
        _lastMessageTime = [dict objectForKey:@"added_time"];
        _friendID = [[dict objectForKey:@"friend_id"] intValue];
    }
    return self;
}

@end
