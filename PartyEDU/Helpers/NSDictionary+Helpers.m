

#import "NSDictionary+Helpers.h"

@implementation NSDictionary (Helpers)
-(BOOL)hasProperty:(NSString*)key
{
    id object = [self objectForKey:key];
    
    if (object == nil)
    {
        return NO;
    }
    
    return YES;
}

-(id)customObjectForKey:(NSString*)key
{
    id object = [self objectForKey:key];
    
    if (object == nil || object == NULL || object == [NSNull null])
    {
        return nil;
    }
    
    return object;
}

-(NSString*)stringForKey:(NSString*)key
{
    id object = [self customObjectForKey:key];
    
    if (!object)
    {
        return @"";
    }
    
    return [NSString stringWithFormat:@"%@",object];
}

-(NSInteger)intForKey:(NSString*)key
{
    
    id object = [self customObjectForKey:key];
    
    if (!object)
    {
        return 0;
    }
    
    return [object intValue];
}

@end
