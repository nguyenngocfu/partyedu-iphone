//
//  PEMyProfileViewController.h
//  PartyEDU
//
//  Created by thinhpham on 12/28/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PEMyProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewProfile;
@property (strong, nonatomic) UserModel *userInfo;
@property (weak, nonatomic) IBOutlet UIImageView *userImageProfile;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *shortDescriptionLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *collegeLabel;

@end
