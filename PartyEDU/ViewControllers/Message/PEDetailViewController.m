//
//  PEDetailViewController.m
//  PartyEDU
//
//  Created by thinhpham on 2/22/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PEDetailViewController.h"
#import "MFSideMenu.h"
#import "NSObject+SBJson.h"
#import "Appclient.h"
#import "AFNetworking.h"
#import "PEPartyViewController.h"
#import "PECustomChatFriendCell.h"
#import "PECustomChatMeCell.h"
#import "MessageModel.h"
#import "UserModel.h"

@interface PEDetailViewController (){
    NSMutableArray *arrayMessage;
    YFInputBar *inputBar;
    UserModel *userInfo;
}

@end

@implementation PEDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayMessage = [[NSMutableArray alloc]init];
        _dbHelper = [[DBHelper alloc]init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    [self customKeyboard];
    userInfo = [[UserModel alloc] initWithDictionary:[[Helpers shared] getUserInfo]];
    _labelFriendName.text = [NSString stringWithFormat:@"%@ %@", userInfo.firstName, userInfo.lastName];
    [myAppDelegate setIsCurrentMessageScreen:YES];
    [self getListMessage];
}
-(void)viewWillDisappear:(BOOL)animated{
    [myAppDelegate setIsCurrentMessageScreen:NO];
}

- (void)viewWillAppear:(BOOL)animated{
    
}

#pragma mark Custom NavigationBar

- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"icon_Location.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}
- (void)rightSideMenuButtonPressed:(id)sender {
    
    // Push to Myparties view
    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
    PEPartyViewController *parties = [[PEPartyViewController alloc]initWithNibName:@"PEPartyViewController" bundle:nil];
    [parties getCurrentLocation];
    [[slideMenu centerViewController] pushViewController:parties animated:NO];
}

#pragma mark getListMessage

-(void)getListMessage{
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    [arrayMessage removeAllObjects];
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getListMessageHistory:(int)[[Helpers shared] getIntegerForKey:USER_ID]  andFriendID:_friendInfo.friendID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
//            for (NSDictionary *dict in eventArr) {
//                MessageModel *message = [[MessageModel alloc]initWithDictionary:dict];
//                [arrayMessage addObject:message];
//            }
            for (int i = (int)eventArr.count-1; i >=0; i--) {
                MessageModel *message = [[MessageModel alloc]initWithDictionary:[eventArr objectAtIndex:i]];
                [arrayMessage addObject:message];
            }
            // save lastMessage to DB
            if (arrayMessage.count>0) {
                [self saveLastMessage];
            }

            [_tableMessageDetail reloadData];
            if (_tableMessageDetail.contentSize.height > _tableMessageDetail.frame.size.height)
            {
                CGPoint offset = CGPointMake(0, _tableMessageDetail.contentSize.height -     _tableMessageDetail.frame.size.height);
                [_tableMessageDetail setContentOffset:offset animated:NO];
            }
            
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}

-(void)saveLastMessage{
    MessageModel *lastMessage = [arrayMessage lastObject];
    MessageModel *message = [[MessageModel alloc]init];
    message.friendID = _friendInfo.friendID;
    message.friendName = _friendInfo.friendName;
    message.userID = userInfo.userID;
    message.lastMessage = lastMessage.lastMessage;
    message.lastMessageTime = lastMessage.lastMessageTime;
    message.friendPhoto = lastMessage.friendPhoto;
//    [self getFriendPhoto:lastMessage];
    message.friendEmail = lastMessage.friendEmail;
    if ([_dbHelper hasDataRecordsWithFriendID:_friendInfo.friendID]) {
        [_dbHelper deleteMessageWithFriendID:_friendInfo.friendID];
    }
    [_dbHelper addNewMessage:message];
    

}

#pragma -mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayMessage.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return ([[Helpers shared] getHeightFromText:[[arrayMessage objectAtIndex:indexPath.row] lastMessage]]+ 15 +25 );
//    return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *cellIndentifier = @"PECustomMyPartyCell";
    NSLog(@"friend name: %@",[[arrayMessage objectAtIndex:indexPath.row] friendName] );
    NSLog(@"message name: %@",[[arrayMessage objectAtIndex:indexPath.row] lastMessage] );
    
    
    if ([[[arrayMessage objectAtIndex:indexPath.row] friendName] isEqualToString:[NSString stringWithFormat:@"%@ %@", userInfo.firstName, userInfo.lastName]]) {
        NSLog(@"cell me");
        PECustomChatMeCell *cell = (PECustomChatMeCell*)[tableView dequeueReusableCellWithIdentifier:@"PECustomChatMeCell"];
        if (cell == nil) {
            cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECustomChatMeCell" owner:self options:nil] objectAtIndex:0];
        }
        cell.textMessage.text = [[arrayMessage objectAtIndex:indexPath.row] lastMessage];
        cell.dateTime.text = [[arrayMessage objectAtIndex:indexPath.row] lastMessageTime];
        
        [cell.avartaImage setImageWithURL:[NSURL URLWithString:[[arrayMessage objectAtIndex:indexPath.row] friendPhoto]] placeholderImage:[UIImage imageNamed:@"no_image"]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    PECustomChatFriendCell *cell = (PECustomChatFriendCell*)[tableView dequeueReusableCellWithIdentifier:@"PECustomChatFriendCell"];
    NSLog(@"cell friend");
    
    if (cell == nil) {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECustomChatFriendCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.textMessage.text = [[arrayMessage objectAtIndex:indexPath.row] lastMessage];
    cell.dateTime.text = [[arrayMessage objectAtIndex:indexPath.row] lastMessageTime];
    
    [cell.avartaImage setImageWithURL:[NSURL URLWithString:[[arrayMessage objectAtIndex:indexPath.row] friendPhoto]] placeholderImage:[UIImage imageNamed:@"no_image"]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark Custom keyboard

-(void)customKeyboard{
    inputBar = [[YFInputBar alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY([UIScreen mainScreen].bounds)-108, 320, 44)];
    
    inputBar.backgroundColor = RGB(205, 55, 54);
    
    
    inputBar.delegate = self;
    inputBar.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    inputBar.textField.borderStyle = UITextBorderStyleRoundedRect;
    inputBar.resignFirstResponderWhenSend = YES;
    inputBar.textField.delegate = self;
    
    
    [self.view addSubview:inputBar];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
-(void)showKeyBoard{
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
    
    
    
}
-(void)hiddenKeyBoard{
    [UIView animateWithDuration:0.3
                          delay:0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
    
    
}
-(void)addComment{
    [self.view endEditing:YES];
    if ([inputBar.textField.text isEqualToString:@""]) {
        return;
    }
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    MBProgressShowing();
    [arrayMessage removeAllObjects];
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client sendMessageWithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] andFriendID:_friendInfo.friendID message:inputBar.textField.text success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            inputBar.textField.text = @"";
            
            for (int i = (int)eventArr.count-1; i >=0; i--) {
                MessageModel *message = [[MessageModel alloc]initWithDictionary:[eventArr objectAtIndex:i]];
                [arrayMessage addObject:message];
            }
            // save lastMessage to DB
            if (arrayMessage.count>0) {
                [self saveLastMessage];
            }
            
            [_tableMessageDetail reloadData];
            if (_tableMessageDetail.contentSize.height > _tableMessageDetail.frame.size.height)
            {
                CGPoint offset = CGPointMake(0, _tableMessageDetail.contentSize.height -     _tableMessageDetail.frame.size.height);
                [_tableMessageDetail setContentOffset:offset animated:NO];
            }

            
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}


@end
