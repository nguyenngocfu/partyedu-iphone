//
//  EventModel.h
//  PartyEDU
//
//  Created by thinhpham on 1/3/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"

@interface EventModel : NSObject

@property (assign, nonatomic) int userID;
@property (assign, nonatomic) int eventID;
@property (assign, nonatomic) int categoryID;
@property (strong, nonatomic) NSString *categoryName;
@property (strong, nonatomic) NSString *eventName;
@property (strong, nonatomic) NSString *eventDate;
@property (strong, nonatomic) NSString *eventTime;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *stateEvent;
@property (strong, nonatomic) NSString *zip_code;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *cover;
@property (strong, nonatomic) NSString *imageEvent;
@property (strong, nonatomic) NSString *collage;
@property (strong, nonatomic) NSString *otherDetail;
@property (strong, nonatomic) NSString *latitue;
@property (strong, nonatomic) NSString *longtitue;
@property (strong, nonatomic) NSString *userName;

- (id)initWithDictionary:(NSDictionary *)dict;
@end
