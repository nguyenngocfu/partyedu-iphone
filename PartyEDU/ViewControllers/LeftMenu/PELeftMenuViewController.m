//
//  PELeftMenuViewController.m
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PELeftMenuViewController.h"
#import "PEHomeViewController.h"
#import "PEMyProfileViewController.h"
#import "PEMyPartiesViewController.h"
#import "PEAttendingViewController.h"
#import "PEFriendViewController.h"
#import "PEEventViewController.h"
#import "PESettingViewController.h"
#import "PEMessageViewController.h"
#import "PEPartyInviteViewController.h"
#import "PESearchResultViewController.h"
#import "PEPendingRequestViewController.h"
#import "UserModel.h"
#import "UIImageView+AFNetworking.h"

@interface PELeftMenuViewController ()<UISearchBarDelegate>{
    NSArray *arrayTitle;
    NSArray *arrayImage;
    
}

@end

@implementation PELeftMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrayTitle = @[@"Profile", @"Messages", @"Friends", @"Find Party",@"My Parties", @"Party Invites", @"Create Party", @"Attending", @"Pending Request", @"Sign Out"];
    arrayImage = @[@"icon_User", @"icon_Message", @"icon_Friend", @"icon_find",@"icon_Party", @"icon_Invite",@"icon_CreateParty", @"icon_Attending",@"icon_Pending", @"icon_SignOut"];
    // Do any additional setup after loading the view from its nib.
    _rowSelected = -1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayTitle.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 46;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifier = @"PECustomMenuCell";
    PECustomMenuCell *cell = (PECustomMenuCell*)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil) {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECustomMenuCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.titleCell.text = [arrayTitle objectAtIndex:indexPath.row];
    
    if (indexPath.row == 0) {
        UserModel *userInfo = [[UserModel alloc] initWithDictionary:[[Helpers shared] getUserInfo]];
        [cell.imageCell setImageWithURL:[NSURL URLWithString:userInfo.avataLink] placeholderImage:[UIImage imageNamed:@"avatar.png"]];
        cell.imageCell.layer.cornerRadius = cell.imageCell.frame.size.width/2;
//        cell.imageCell.layer.borderWidth = 1;
//        cell.imageCell.layer.borderColor = [UIColor blackColor].CGColor;
        cell.imageCell.contentMode = UIViewContentModeScaleAspectFill;
    } else {
        cell.imageCell.image = [UIImage imageNamed:[arrayImage objectAtIndex:indexPath.row]];
    }
    
    if (_rowSelected == indexPath.row) {
        cell.contentView.backgroundColor = RGB(220, 59, 59);
        cell.titleCell.textColor = [UIColor whiteColor];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_rowSelected == indexPath.row) {
        [self showRightMenu];
        return;
    } else {
        _rowSelected = (int)indexPath.row;
        [[[myAppDelegate slideMenu] centerViewController] popToRootViewControllerAnimated:NO];
    }
   
    switch (indexPath.row) {
        case 0:
        {
            PEMyProfileViewController *profileView = [[PEMyProfileViewController alloc]initWithNibName:@"PEMyProfileViewController" bundle:nil];
            [[[myAppDelegate slideMenu] centerViewController] pushViewController:profileView animated:NO];
        }
            break;
        case 1:
        {
            PEMessageViewController *messageView = [[PEMessageViewController alloc]initWithNibName:@"PEMessageViewController" bundle:nil];
            [[[myAppDelegate slideMenu] centerViewController] pushViewController:messageView animated:NO];
        }
            break;
        case 2:
        {
            PEFriendViewController *friendView = [[PEFriendViewController alloc]initWithNibName:@"PEFriendViewController" bundle:nil];
            [[[myAppDelegate slideMenu] centerViewController] pushViewController:friendView animated:NO];
        }
            break;
        case 3:
        {
            PEHomeViewController *homeView = [[PEHomeViewController alloc]initWithNibName:@"PEHomeViewController" bundle:nil];
            [[[myAppDelegate slideMenu] centerViewController] pushViewController:homeView animated:NO];
        }
            break;
        case 4:
        {
            PEMyPartiesViewController *partyView = [[PEMyPartiesViewController alloc]initWithNibName:@"PEMyPartiesViewController" bundle:nil];
            [[[myAppDelegate slideMenu] centerViewController] pushViewController:partyView animated:NO];
        }
            break;
        case 5:
        {
            PEPartyInviteViewController *partyInviteView = [[PEPartyInviteViewController alloc]initWithNibName:@"PEPartyInviteViewController" bundle:nil];
            [[[myAppDelegate slideMenu] centerViewController] pushViewController:partyInviteView animated:NO];
        }
            break;
        case 6:
        {
            PEEventViewController *eventView = [[PEEventViewController alloc]initWithNibName:@"PEEventViewController" bundle:nil];
            [[[myAppDelegate slideMenu] centerViewController] pushViewController:eventView animated:NO];
        }
            break;
        case 7:
        {
            PEAttendingViewController *attendingView = [[PEAttendingViewController alloc]initWithNibName:@"PEAttendingViewController" bundle:nil];
            [[[myAppDelegate slideMenu] centerViewController] pushViewController:attendingView animated:NO];
        }
            break;
        case 8:
        {
            PEPendingRequestViewController *pendingView = [[PEPendingRequestViewController alloc]initWithNibName:@"PEPendingRequestViewController" bundle:nil];
            [[[myAppDelegate slideMenu] centerViewController] pushViewController:pendingView animated:NO];
        }
            break;
        
        default:
        {
            [self backToLogin];
        }
            break;
    }
    [tableView reloadData];
    [self showRightMenu];
    
}

-(void)showRightMenu{
    [[myAppDelegate slideMenu] toggleLeftSideMenuCompletion:^{
        
    }];
}
-(void)backToLogin{
    
    // logout
    [[Helpers shared] saveInteger:0 forKey:USER_ID];
    [[myAppDelegate nav]popViewControllerAnimated:YES];
}

#pragma mark - SearchBar Delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length != 0) {
        
        [self searchWithString:searchBar.text];
        // Hide keyboard and show results
        [searchBar resignFirstResponder];
    }
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    // Limit user's input to English letters and numbers
    NSCharacterSet *legalCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789 \n"];
    NSCharacterSet *illegalCharacterSet = [legalCharacterSet invertedSet];
    
    NSRange r = [text rangeOfCharacterFromSet:illegalCharacterSet];
    if (r.location != NSNotFound) {
        return NO;
    } else {
        return YES;
    }
}

- (void)searchWithString:(NSString *)query
{
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{
    
}

#pragma mark - Search Controller
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    
    [self searchWithString:searchText];
}
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return NO;
}

- (IBAction)buttonSearchClick:(id)sender {
    [self.view endEditing:YES];
    if ([_searchTextField.text isEqualToString:@""]) {
        [[Helpers shared] alertStatus:@"Please fill text to search" title:@"" delegate:nil];
        return;
    }
    _rowSelected = -1;
    [[[myAppDelegate slideMenu] centerViewController] popToRootViewControllerAnimated:NO];
    PESearchResultViewController *searchResultView = [[PESearchResultViewController alloc]initWithNibName:@"PESearchResultViewController" bundle:nil];
    [[[myAppDelegate slideMenu] centerViewController] pushViewController:searchResultView animated:NO];
    [searchResultView getListSearchResult:_searchTextField.text];
    [self showRightMenu];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

@end
