//
//  EventModel.m
//  PartyEDU
//
//  Created by thinhpham on 1/3/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "EventModel.h"

@implementation EventModel

- (id)initWithDictionary:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        _eventID = [[dict objectForKey:@"event_id"] intValue];
        _eventName = [dict objectForKey:@"event_name"];
        _categoryID = [[dict objectForKey:@"category_id"] intValue];
        _categoryName = [dict objectForKey:@"category_name"];
        _eventDate = [dict objectForKey:@"event_date"];
        _eventTime = [dict objectForKey:@"event_time"];
        _address = [dict objectForKey:@"address"];
        _city = [dict objectForKey:@"city"];
        _stateEvent = [dict objectForKey:@"state"];
        _cover = [dict objectForKey:@"cover"];
        _collage = [dict objectForKey:@"college_affilate"];
        _otherDetail = [dict objectForKey:@"other_details"];
        _latitue = [dict objectForKey:@"latitude"];
        _longtitue = [dict objectForKey:@"longitude"];
        _imageEvent = [dict objectForKey:@"event_image"];
        _phone = [dict objectForKey:@"phone"];
        _email = [dict objectForKey:@"email"];
        _zip_code = [dict objectForKey:@"zip_code"];
        _userID = [[dict objectForKey:@"user_id"] intValue];
        _userName = [dict objectForKey:@"user_name"];
    }
    return self;
}
@end
