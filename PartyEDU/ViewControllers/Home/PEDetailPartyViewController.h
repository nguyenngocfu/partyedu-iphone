//
//  PEDetailPartyViewController.h
//  PartyEDU
//
//  Created by thinhpham on 1/20/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "EventModel.h"
#import "YFInputBar.h"

@interface PEDetailPartyViewController : UIViewController<GMSMapViewDelegate, YFInputBarDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageDetailParty;

@property (weak, nonatomic) IBOutlet UIView *viewCover;
@property (strong, nonatomic) EventModel *eventSelected;

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *categoryIcon;

@property (weak, nonatomic) IBOutlet UILabel *eventNameLabel;

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *buttonJoin;
@property (weak, nonatomic) IBOutlet UIView *viewAttending;
@property (weak, nonatomic) IBOutlet UIView *viewDetail;
@property (weak, nonatomic) IBOutlet UIView *viewComment;
@property (weak, nonatomic) IBOutlet UITableView *tableDetailEvent;
@property (weak, nonatomic) IBOutlet UITableView *tableAttendingView;
@property (weak, nonatomic) IBOutlet UITableView *tableCommentView;
- (IBAction)showViewContent:(id)sender;



@end
