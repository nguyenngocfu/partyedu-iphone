//
//  PEDetailViewController.h
//  PartyEDU
//
//  Created by thinhpham on 2/22/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YFInputBar.h"
#import "FriendModel.h"
#import "DBHelper.h"

@interface PEDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, YFInputBarDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableMessageDetail;
@property (weak, nonatomic) IBOutlet UILabel *labelFriendName;
@property (strong, nonatomic) FriendModel *friendInfo;
@property (strong, nonatomic) DBHelper *dbHelper;
@end
