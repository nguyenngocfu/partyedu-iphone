//
//  PECustomMenuCell.h
//  PartyEDU
//
//  Created by thinhpham on 12/28/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PECustomMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageCell;
@property (weak, nonatomic) IBOutlet UILabel *titleCell;

@end
