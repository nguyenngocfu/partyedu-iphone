//
//  UserModel.h
//  PartyEDU
//
//  Created by Pham Van Thinh on 1/20/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject
@property (assign, nonatomic) int userID;
@property (strong, nonatomic) NSString *avataLink;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *college;
@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) NSString *Description;
@property (strong, nonatomic) NSString *sortDescription;

- (id)initWithDictionary:(NSDictionary *)dict;
@end

