//
//  LocationTracking.m
//  GiftCard
//
//  Created by Pham Van Thinh on 6/25/14.
//  Copyright (c) 2014 Pham Van Thinh. All rights reserved.
//

#import "LocationTracking.h"
static LocationTracking *location = nil;
@implementation LocationTracking
- (id)init{
    self = [super init];
    if (self) {
        
        self.delegate = self;
        [self setDesiredAccuracy:kCLLocationAccuracyBest];
    }
    
    return self;
}

-(void)updateLocation
{
    
    if ([CLLocationManager locationServicesEnabled])
    {
        [super startUpdatingLocation];
    }else
    {
        [[Helpers shared] alertStatus:@"Please turn on Location Service!" title:@"" delegate:nil];
        if ([_delegateLocation respondsToSelector:@selector(failedGetLocation)]) {
            [_delegateLocation failedGetLocation];
        }
        
    }
}

-(void)stopUpdatingLocation
{
    [super stopUpdatingLocation];
}


#pragma mark - CLLocationManagerDelegate

// Location Manager Delegate Methods for iOS8+
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
//    NSLog(@"%@", [locations lastObject]);
    CLLocation *newLocation = [locations lastObject];
    [self.locationManager stopUpdatingLocation];
    
    // Return if we already have a location or if the returned location is invaled
    if (self.currentLocation || newLocation.horizontalAccuracy < 0)
        return;
    self.currentLocation = newLocation;
    if (_checkGetPostCode) {
        [self geocodeLocation:newLocation];
    } else {
        if ([_delegateLocation respondsToSelector:@selector(getLocation:)]) {
            [_delegateLocation getLocation:newLocation];
        }
    }
}

// for IOS7
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    [self.locationManager stopUpdatingLocation];
    
    // Return if we already have a location or if the returned location is invaled
    if (self.currentLocation || newLocation.horizontalAccuracy < 0)
        return;
    self.currentLocation = newLocation;
    if (_checkGetPostCode) {
        [self geocodeLocation:newLocation];
    } else {
        if ([_delegateLocation respondsToSelector:@selector(getLocation:)]) {
            [_delegateLocation getLocation:newLocation];
        }
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if ([_delegateLocation respondsToSelector:@selector(failedGetLocation)]) {
        [_delegateLocation failedGetLocation];
    }
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        NSLog(@"Please authorize location services");
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorized)
        [self.locationManager startUpdatingLocation];
    else if (status == kCLAuthorizationStatusDenied){
        if ([_delegateLocation respondsToSelector:@selector(failedGetLocation)]) {
            [_delegateLocation failedGetLocation];
        }
    }
        
}

#pragma mark - get my location

- (void)geocodeLocation:(CLLocation *)location {
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if(!error){
            [placemarks enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                CLPlacemark *aPlacemark = (CLPlacemark *)obj;
                NSString *postCode = aPlacemark.postalCode;
                if ([postCode isEqualToString:@"nil"]) {
                    postCode = @"";
                }
                
                if ([_delegateLocation respondsToSelector:@selector(getPostCode:andLocation:)]) {
                    [_delegateLocation getPostCode:postCode andLocation:location];
                }
                
            }];
            
        }
        else{
            NSLog(@"failed getting city: %@", [error description]);
            
        }
        
    }];
}



@end
