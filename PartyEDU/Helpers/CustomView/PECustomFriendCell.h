//
//  PECustomFriendCell.h
//  PartyEDU
//
//  Created by thinhpham on 12/29/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomFriendDelegate<NSObject>
@optional;
-(void) showMessageDetail:(NSInteger) tag;
-(void) showPopupInviteParty:(NSInteger) tag;
-(void) acceptFriend:(NSInteger) tag;
-(void) denyFriend:(NSInteger) tag;
@end

@interface PECustomFriendCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_avarta;
@property (weak, nonatomic) IBOutlet UILabel *titleUserName;
@property (weak, nonatomic) IBOutlet UIButton *buttonAccept;
@property (weak, nonatomic) IBOutlet UIButton *buttonMessage;
@property (weak, nonatomic) IBOutlet UIButton *buttonInvite;
@property (weak, nonatomic) IBOutlet UIButton *buttonDeny;
@property (weak, nonatomic) IBOutlet UILabel *favoriteDrinkLabel;
@property (weak, nonatomic) IBOutlet UILabel *collegeLabel;

@property (weak, nonatomic) id <CustomFriendDelegate> delegate;
- (IBAction)buttonInviteClick:(id)sender;
- (IBAction)buttonMessageClick:(id)sender;
- (IBAction)buttonAcceptClick:(id)sender;
- (IBAction)buttonDenyClick:(id)sender;

@end
