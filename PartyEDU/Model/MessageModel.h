//
//  MessageModel.h
//  PartyEDU
//
//  Created by thinhpham on 2/21/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageModel : NSObject
@property (assign, nonatomic) int _id;
@property (assign, nonatomic) int userID;
@property (assign, nonatomic) int friendID;
@property (strong, nonatomic) NSString *friendName;
@property (strong, nonatomic) NSString *friendEmail;
@property (strong, nonatomic) NSString *friendPhoto;
@property (strong, nonatomic) NSString *lastMessage;
@property (strong, nonatomic) NSString *lastMessageTime;

- (id)initWithDictionary:(NSDictionary *)dict;
@end
