//
//  YFInputBar.m
//  test
//
//  Created by 杨峰 on 13-11-10.
//  Copyright (c) 2013年 杨峰. All rights reserved.
//

#import "YFInputBar.h"
#import "PEAppDelegate.h"
@implementation YFInputBar


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor grayColor];
        
        self.frame = CGRectMake(0, CGRectGetMinY(frame), 320, CGRectGetHeight(frame));
        
        self.textField.tag = 10000;
        

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}
-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    _originalFrame = frame;
}
//_originalFrame
-(void)setOriginalFrame:(CGRect)originalFrame
{
    self.frame = CGRectMake(0, CGRectGetMinY(originalFrame), 320, CGRectGetHeight(originalFrame));
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark get textfield
-(UITextField *)textField
{
    if (!_textField) {
        
        
        
        _textField = [[UITextField alloc]initWithFrame:CGRectMake(8, 8, 250, 28)];
        _textField.backgroundColor = [UIColor whiteColor];
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.delegate = self;
        UIButton *buttonAddComment = [[UIButton alloc]initWithFrame:CGRectMake(260, 8, 60, 28)];
        [buttonAddComment addTarget:self action:@selector(addComment:) forControlEvents:UIControlEventTouchUpInside];
        [buttonAddComment setTitle:@"Send" forState:UIControlStateNormal];
        [buttonAddComment setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:buttonAddComment];
        
        [self addSubview:_textField];
    }
    return _textField;
}


-(IBAction)addComment:(id)sender{
    if ([_delegate respondsToSelector:@selector(addComment)]) {
        [_delegate addComment];
    }
}
#pragma mark keyboardNotification

- (void)keyboardWillShow:(NSNotification*)notification{
    CGRect _keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"%f-%f-%f-%f",_keyboardRect.origin.y,_keyboardRect.size.height,[self getHeighOfWindow]-CGRectGetMaxY(self.frame),CGRectGetMinY(self.frame));
    

    if ([self convertYToWindow:CGRectGetMaxY(self.originalFrame)]>=_keyboardRect.origin.y)
    {

        if (self.frame.origin.y== self.originalFrame.origin.y) {
            
            [UIView animateWithDuration:0.25
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 self.transform = CGAffineTransformMakeTranslation(0, -_keyboardRect.size.height+[self getHeighOfWindow]-CGRectGetMaxY(self.originalFrame)-64);
                             } completion:nil];
        }
        else
        {
            self.transform = CGAffineTransformMakeTranslation(0, -_keyboardRect.size.height+[self getHeighOfWindow]-CGRectGetMaxY(self.originalFrame));
        }
        
    }
    else
    {
        
    }
    if ([_delegate respondsToSelector:@selector(showKeyBoard)]) {
        [_delegate showKeyBoard];
    }
    
    
}

- (void)keyboardWillHide:(NSNotification*)notification{
    

    [UIView animateWithDuration:0.25
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.transform = CGAffineTransformMakeTranslation(0, 0);
                     } completion:nil];
    if ([_delegate respondsToSelector:@selector(hiddenKeyBoard)]) {
        [_delegate hiddenKeyBoard];
    }
}
#pragma  mark ConvertPoint

-(float)convertYFromWindow:(float)Y
{
    PEAppDelegate *appDelegate = (PEAppDelegate*)[UIApplication sharedApplication].delegate;
    CGPoint o = [appDelegate.window convertPoint:CGPointMake(0, Y) toView:self.superview];
    return o.y;
    
}
-(float)convertYToWindow:(float)Y
{
    PEAppDelegate *appDelegate = (PEAppDelegate*)[UIApplication sharedApplication].delegate;
    CGPoint o = [self.superview convertPoint:CGPointMake(0, Y) toView:appDelegate.window];
    return o.y;
    
}
-(float)getHeighOfWindow
{
    PEAppDelegate *appDelegate = (PEAppDelegate*)[UIApplication sharedApplication].delegate;
    return appDelegate.window.frame.size.height;
}



-(BOOL)resignFirstResponder
{
    [self.textField resignFirstResponder];
    return [super resignFirstResponder];
}
@end
