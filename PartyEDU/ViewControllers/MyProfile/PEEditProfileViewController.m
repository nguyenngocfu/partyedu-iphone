//
//  PEEditProfileViewController.m
//  PartyEDU
//
//  Created by Pham Van Thinh on 1/21/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PEEditProfileViewController.h"
#import "Appclient.h"
#import "NSObject+SBJson.h"

@interface PEEditProfileViewController (){
    UIImage *imageSelected;
}

@end

@implementation PEEditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _scrollViewProfile.frame = CGRectMake(0, 64, 320, [[UIScreen mainScreen] bounds ].size.height-64);
    _scrollViewProfile.contentSize = CGSizeMake(320, 525);
    _viewInfoUser.backgroundColor = [UIColor whiteColor];
    _viewInfoUser.layer.cornerRadius = 8;
    _descriptionTextView.delegate = self;
    
    [self setUpInfoUser];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpInfoUser{
    _firstNameTextField.text = _userModel.firstName;
    _lastNameTextField.text = _userModel.lastName;
    _emailTextField.text = _userModel.email;
    _collegeTextField.text = _userModel.college;
    _shortDescriptionTextField.text = _userModel.sortDescription;
    _descriptionTextView.text = _userModel.Description;
    [_descriptionTextView customPlaceholderLabel: @"Description"];
}


#pragma mark uploadImage

- (IBAction)buttonChooseImageClick:(id)sender {
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:@"Upload photo"
                                      delegate:self
                                      cancelButtonTitle:@"Cancel"
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:@"Take Photo", @"Existing Photo", nil];
    [actionSheet showInView:self.view];
    actionSheet.delegate = self;
        
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        [self takeCamera:self];
    }else  if (buttonIndex == 1) {
        [self takePhoto:self];
    }
}
    
-(IBAction)takeCamera:(id)sender{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
    }
}
-(IBAction)takePhoto:(id)sender{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}
    
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)imageInfo
{
    [self dismissViewControllerAnimated:YES completion:nil];
    imageSelected = [imageInfo valueForKey:UIImagePickerControllerOriginalImage];
}
    
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)uploadImage{
    MBProgressShowing();
    NSURL *url=[NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"user", @"mode",
                            @(_userModel.userID),@"id",
                            nil];
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"partyedu/api/photo_upload.php" parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        NSDate *start = [NSDate date];
        NSTimeInterval timeInterval = [start timeIntervalSince1970];
        NSString *fileName = [NSString stringWithFormat:@"%f.png",timeInterval];
        [formData appendPartWithFileData: UIImageJPEGRepresentation(imageSelected, 0.8) name:@"uploaded_file" fileName:fileName mimeType:@"file"];
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error;
        //        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        MBProgressHiding();
        
        NSDictionary* results = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&error];
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            
            // update image avarta
            NSDictionary *dictImage = [results objectForKey:@"details"];
            NSString *photoLink = [dictImage objectForKey:@"photoName"];
            NSMutableDictionary *dictUser = [[[Helpers shared] getUserInfo] mutableCopy];
//            BOOL checkHavePhotoName = NO;
            [dictUser setValue:photoLink forKey:@"photoName"];
            
//            for (id key  in [dictUser allKeys]) {
//                if ([key isEqualToString:@"photoName"]) {
//                    [dictUser setValue:photoLink forKey:key];
//                    checkHavePhotoName = YES;
//                    break;
//                }
//            }
//            if (!checkHavePhotoName) {
//                [dictUser ]
//            }
            
            [[Helpers shared] saveUserInfo:dictUser];
            
            PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
            PELeftMenuViewController *leftMenu = (PELeftMenuViewController *)[slideMenu leftMenuViewController];
            [leftMenu.tableMenu reloadData];

            
            
            [self updateProfileSuccess];
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
 
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        MBProgressHiding();
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
    
    [httpClient enqueueHTTPRequestOperation:operation];
    
}

#pragma mark update profile
- (IBAction)buttonSaveClick:(id)sender {
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    
    MBProgressShowing();
    _userModel.firstName = _firstNameTextField.text ;
     _userModel.lastName = _lastNameTextField.text;
     _userModel.email = _emailTextField.text;
    _userModel.college = _collegeTextField.text;
    _userModel.sortDescription = _shortDescriptionTextField.text;
    _userModel.Description = _descriptionTextView.text;
    
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client updateUserProfile:_userModel success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSMutableDictionary *dict = [results objectForKey:@"details"];
            [dict setObject:_userModel.avataLink forKey:@"photoName"];
            [[Helpers shared] saveUserInfo:dict];
            if (imageSelected) {
                [self uploadImage];
            } else {
                [self updateProfileSuccess];
            }
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];

}
- (void)updateProfileSuccess{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
