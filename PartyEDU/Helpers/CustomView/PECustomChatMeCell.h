//
//  PECustomChatMeCell.h
//  PartyEDU
//
//  Created by thinhpham on 2/22/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PECustomChatMeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avartaImage;
@property (weak, nonatomic) IBOutlet UILabel *textMessage;
@property (weak, nonatomic) IBOutlet UILabel *dateTime;

@end
