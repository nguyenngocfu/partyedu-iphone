//
//  PEDetailFriendViewController.h
//  PartyEDU
//
//  Created by thinhpham on 2/18/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@interface PEDetailFriendViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewProfile;
@property (strong, nonatomic) UserModel *userInfo;
@property (assign, nonatomic) int checkFriend;
@property (assign, nonatomic) int friend_id;
@property (weak, nonatomic) IBOutlet UIImageView *userImageProfile;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *shortDescriptionLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *collegeLabel;

@end
