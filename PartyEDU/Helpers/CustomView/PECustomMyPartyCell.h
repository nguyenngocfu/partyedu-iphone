//
//  PECustomMyPartyCell.h
//  PartyEDU
//
//  Created by thinhpham on 12/30/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PECustomMyPartyCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *img_avarta;
@property (weak, nonatomic) IBOutlet UILabel *titleParty;
@property (weak, nonatomic) IBOutlet UILabel *titleAddress;
@property (weak, nonatomic) IBOutlet UILabel *titleTime;

@end
