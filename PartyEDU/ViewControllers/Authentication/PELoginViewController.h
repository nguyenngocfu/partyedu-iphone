//
//  PELoginViewController.h
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PERegisterViewController.h"
#import "PEForgotPasswordViewController.h"
#import"MCustomTextfield.h"

@interface PELoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewLogin;
@property (weak, nonatomic) IBOutlet MCustomTextfield *emailTextField;
@property (weak, nonatomic) IBOutlet MCustomTextfield *passTextField;

- (IBAction)buttonLoginClick:(id)sender;
- (IBAction)buttonRegisterClick:(id)sender;
- (IBAction)buttonForgotPasswordClick:(id)sender;

@end
