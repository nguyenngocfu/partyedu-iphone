//
//  CustomLabelPadding.m
//  PartyEDU
//
//  Created by thinhpham on 2/23/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "CustomLabelPadding.h"

@implementation CustomLabelPadding


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    UIEdgeInsets insets = {0, 10, 0, 5};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
