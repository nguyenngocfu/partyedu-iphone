//
//  PEPendingRequestViewController.h
//  PartyEDU
//
//  Created by Pham Van Thinh on 2/16/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PECustomFriendCell.h"

@interface PEPendingRequestViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, CustomFriendDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableFriend;
@end
