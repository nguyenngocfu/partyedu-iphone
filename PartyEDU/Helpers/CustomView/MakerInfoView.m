//
//  MakerInfoView.m
//  PartyEDU
//
//  Created by Phạm Văn Thịnh on 1/6/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "MakerInfoView.h"
#import "UIImageView+AFNetworking.h"
#define CALLOUTVIEW_WIDTH 295
#define  CALLOUTVIEW_HEIGHT 229

@implementation MakerInfoView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithEvent:(EventModel*)event
{
    self = [self initWithFrame:CGRectMake(0, 0, CALLOUTVIEW_WIDTH, CALLOUTVIEW_HEIGHT)];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        UIImageView* bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CALLOUTVIEW_WIDTH, CALLOUTVIEW_HEIGHT)];
        bgView.image = [UIImage imageNamed:@"bg_CustomInfoView.png"];
        [self addSubview:bgView];
        
        UIImageView* imageEvent = [[UIImageView alloc] initWithFrame:CGRectMake(27, 25, 235, 114)];
//        [imageEvent.layer setBorderColor: [[UIColor whiteColor] CGColor]];
//        imageEvent.layer.borderWidth = 2;
        [imageEvent setImageWithURL:[NSURL URLWithString:event.imageEvent] placeholderImage:[UIImage imageNamed:@"no_pic_available.png"]];
//        imageEvent.image = [UIImage imageNamed:@"no_pic_available.png"];
        imageEvent.contentMode = UIViewContentModeScaleAspectFill;
        imageEvent.clipsToBounds = YES;
        [self addSubview:imageEvent];
        
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 140, 150, 45)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.numberOfLines = 2;
        titleLabel.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:15];
        titleLabel.text = event.eventName;
        titleLabel.textColor = [UIColor whiteColor];
        [self addSubview:titleLabel];
        
        UILabel* dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(175, 140, 105, 21)];
        dateLabel.backgroundColor = [UIColor clearColor];
        dateLabel.textAlignment = NSTextAlignmentLeft;
        dateLabel.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:10];
        dateLabel.text = [NSString stringWithFormat:@"Date: %@",event.eventDate];
        dateLabel.numberOfLines = 2;
        dateLabel.textColor = [UIColor whiteColor];
        [self addSubview:dateLabel];
        
        UILabel* timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(175, 155, 105, 21)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textAlignment = NSTextAlignmentLeft;
        timeLabel.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:10];
        timeLabel.textColor = [UIColor whiteColor];
        timeLabel.numberOfLines = 1;
        timeLabel.text = [NSString stringWithFormat:@"Time: %@",event.eventTime];
        [self addSubview:timeLabel];
    }
    return self;
}

@end
