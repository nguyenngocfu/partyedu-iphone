//
//  PEDetailFriendViewController.m
//  PartyEDU
//
//  Created by thinhpham on 2/18/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PEDetailFriendViewController.h"
#import "MFSideMenu.h"
#import "AFNetworking.h"
#import "NSObject+SBJson.h"
#import "Appclient.h"

@interface PEDetailFriendViewController ()

@end

@implementation PEDetailFriendViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    _scrollViewProfile.contentSize = CGSizeMake(320, 504);
    _scrollViewProfile.frame = CGRectMake(0, 64, 320, [[UIScreen mainScreen] bounds ].size.height-64);
    
}

- (void)viewWillAppear:(BOOL)animated{
    if (_userInfo) {
        [self setUpUserInfo];
    } else {
        [self getDetailFriend];
    }
    
}
-(void)getDetailFriend{
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }

    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client getDetailUser:(int)[[Helpers shared] getIntegerForKey:USER_ID] andFriendID:_friend_id success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                _userInfo = [[UserModel alloc]initWithDictionary:dict];
            }
            [self setUpUserInfo];
            
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        MBProgressHiding();
    }];
}

-(void)setUpUserInfo{
    _descriptionLabel.text = _userInfo.Description;
    _shortDescriptionLabel.text = _userInfo.sortDescription;
    _nameLabel.text = [NSString stringWithFormat:@"%@ %@",_userInfo.firstName, _userInfo.lastName];
    _collegeLabel.text = _userInfo.college;
    [_userImageProfile setImageWithURL:[NSURL URLWithString:_userInfo.avataLink] placeholderImage:[UIImage imageNamed:@"avatar.png"]];
    _userImageProfile.layer.cornerRadius = _userImageProfile.frame.size.width/2;

}

#pragma mark Custom NavigationBar

- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    rightButton.frame = CGRectMake(0, 0, 100, 30);
    NSString *rightItemTitle;
    
    if (_checkFriend == 0) {
        rightItemTitle = @"Add Friend";
    } else if(_checkFriend == 1) {
        rightItemTitle = @"Block";
    } else {
        rightItemTitle = @"Unblock";
    }
    
    [rightButton setTitle:rightItemTitle forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}
- (void)rightSideMenuButtonPressed:(id)sender {
    if (_checkFriend == 0) {
        [self sendRequestFriend];
    } else if (_checkFriend == 1){
        [self blockUser];
    } else {
        [self unBlockUser];
    }
}
- (void)sendRequestFriend{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client addFriend:(int)[[Helpers shared] getIntegerForKey:USER_ID] andFriendID:_userInfo.userID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            [[Helpers shared] alertPopupStatus:@"Friend request has been sent successfully!" title:@"" delegate:nil];
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
}
- (void)blockUser{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client blockFriend:(int)[[Helpers shared] getIntegerForKey:USER_ID] andFriendID:_userInfo.userID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            _checkFriend = 2;
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            [[Helpers shared] alertPopupStatus:@"Friend request has been blocked successfully!" title:@"" delegate:nil];
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];

}
- (void)unBlockUser{
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client unBlockFriend:(int)[[Helpers shared] getIntegerForKey:USER_ID] andFriendID:_userInfo.userID success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            _checkFriend = 1;
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            [[Helpers shared] alertPopupStatus:@"Friend request has been unblocked successfully!" title:@"" delegate:nil];
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        [[Helpers shared]alertStatus:ERROR_CONECTION title:@"" delegate:nil];
        MBProgressHiding();
    }];
    
}


@end
