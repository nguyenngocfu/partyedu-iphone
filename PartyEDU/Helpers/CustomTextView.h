//
//  CustomTextView.h
//  PartyEDU
//
//  Created by thinhpham on 1/9/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextView : UITextView
{
    UILabel *placeholderLabel;
}
@property (nonatomic) NSInteger leftMargin;
-(void)customPlaceholderLabel:(NSString *)placeHolder;
@end
