//
//  PECustomFriendCell.m
//  PartyEDU
//
//  Created by thinhpham on 12/29/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import "PECustomFriendCell.h"

@implementation PECustomFriendCell

- (void)awakeFromNib
{
    // Initialization code
    _buttonMessage.layer.cornerRadius = 8;
    _buttonInvite.layer.cornerRadius = 8;
    _buttonAccept.layer.cornerRadius = 8;
    _buttonDeny.layer.cornerRadius = 8;
    _img_avarta.layer.cornerRadius = _img_avarta.frame.size.width/2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)buttonInviteClick:(id)sender {
    if ([_delegate respondsToSelector:@selector(showPopupInviteParty:)]) {
        [_delegate showPopupInviteParty:(int)[self tag]];
    }
}

- (IBAction)buttonMessageClick:(id)sender {
    if ([_delegate respondsToSelector:@selector(showMessageDetail:)]) {
        [_delegate showMessageDetail:(int)[self tag]];
    }
}

- (IBAction)buttonAcceptClick:(id)sender {
    if ([_delegate respondsToSelector:@selector(acceptFriend:)]) {
        [_delegate acceptFriend:(int)[self tag]];
    }
}

- (IBAction)buttonDenyClick:(id)sender {
    if ([_delegate respondsToSelector:@selector(denyFriend:)]) {
        [_delegate denyFriend:(int)[self tag]];
    }
}
@end
