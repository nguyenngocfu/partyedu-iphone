//
//  PECommentCell.h
//  PartyEDU
//
//  Created by thinhpham on 2/18/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PECommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avartaUser;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *commentUser;

@end
