#import "Helpers.h"
#import "AppClient.h"
#import "Reachability.h"

static Helpers *helpers = nil;
@implementation Helpers

+ (id)shared
{
    if (!helpers) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            helpers = [[Helpers alloc] init];
        });
    }
    return helpers;
}

#pragma mark set method

-(BOOL)saveObject:(id)object forKey:(NSString *)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:object forKey:key];
    return [userDefault synchronize];
}

-(BOOL)saveInteger:(NSInteger)i forKey:(NSString *)key
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setInteger:i forKey:key];
    return [userDefault synchronize];
}

-(void)saveUserInfo:(NSDictionary *)dictUser{
    for (id key  in [dictUser allKeys]) {
        if (((NSNull *)[dictUser objectForKey:key] == [NSNull null])) {
            [dictUser setValue:@"" forKey:key];
        }
    }
    
    [self saveObject:dictUser forKey:USER_INFO];
}
-(void)saveCategoryList:(NSDictionary *)dictCat{
    for (id key  in [dictCat allKeys]) {
        if (((NSNull *)[dictCat objectForKey:key] == [NSNull null])) {
            [dictCat setValue:@"" forKey:key];
        }
    }
    
    [self saveObject:dictCat forKey:CATEGORY];
}

#pragma mark get method

-(id)getObjectForKey:(NSString*)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:key];
}

-(NSString *)getStringForKey:(NSString*)key{
    return [NSString stringWithFormat:@"%@",[self getObjectForKey:key]];
}

-(NSInteger)getIntegerForKey:(NSString*)key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault integerForKey:key];
}
-(NSString *)getUserToken{
    return [self getStringForKey:USER_TOKEN];
}
-(NSDictionary *)getUserInfo{
    return [self getObjectForKey:USER_INFO];
}
-(NSDictionary *)getCategoryList{
    return [self getObjectForKey:CATEGORY];
}

-(NSString *)getImageFromCategory:(int)index{
    NSString *stringImage;
    switch (index) {
        case 0:
             stringImage = @"";
            break;
            
        default:
            stringImage = @"";
            break;
    }
    return stringImage;
}
//-(GCUserInfoModel *)getUserSenderInfo{
//    if ([self getObjectForKey:USER_INFO]) {
//        GCUserInfoModel *model = [[GCUserInfoModel alloc]initWithDictionary:[self getObjectForKey:USER_INFO]];
//        return model;
//    }
//    return nil;
//}
//-(GCUserInfoModel *)getUserReceiverInfo{
//    if ([self getObjectForKey:USER_RECEIVER]) {
//        GCUserInfoModel *model = [[GCUserInfoModel alloc]initWithDictionary:[self getObjectForKey:USER_RECEIVER]];
//        return model;
//    }
//    return nil;
//}

#pragma mark check network

- (BOOL)checkNetworkAvailable{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

#pragma mark alertView
- (void) alertStatus:(NSString *)msg title:(NSString *)title delegate: (id)delegate
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:delegate
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}
- (void) alertStatusConfirmDelete:(NSString *)msg title:(NSString *)title delegate: (id)delegate
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:delegate
                                              cancelButtonTitle:@"NO"
                                              otherButtonTitles:@"YES", nil];
    [alertView show];
    
}
- (void) alertPopupStatus:(NSString *)msg title:(NSString *)title delegate: (id)delegate
{
    alertViewPopup = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:delegate
                                              cancelButtonTitle:nil
                                              otherButtonTitles:nil, nil];
    [alertViewPopup show];
    [self performSelector:@selector(hideAlertpopup) withObject:nil afterDelay:2];
    
}
-(void)hideAlertpopup{
    [alertViewPopup dismissWithClickedButtonIndex:0 animated:YES];
}
#pragma mark check field valid
- (BOOL)validateEmailWithString:(NSString*)email
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark getHightCell
-(int)getHeightFromText:(NSString *)text{
    UIFont *cellFont = [UIFont fontWithName:@"ArialRoundedMTBold" size:13];
    CGSize constraintSize = CGSizeMake(CELL_TITLE_MAX_WIDTH, MAXFLOAT);
    CGSize labelSize = [text sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
    NSLog(@"line height: %f", labelSize.height);
    CGFloat height = MAX(labelSize.height, 40.0f);
    return height;

}
-(int)getHeightFromTextDetail:(NSString *)text{
    UIFont *cellFont = [UIFont systemFontOfSize:13.0];
    CGSize constraintSize = CGSizeMake(288, MAXFLOAT);
    CGSize labelSize = [text sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
    NSLog(@"line height: %f", labelSize.height);
    CGFloat height = MAX(labelSize.height, 22.0f);
    return height;
    
}

#pragma mark date
-(NSString *)convertDate:(NSString *) timeInterval{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yy"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timeInterval intValue]];
    // Divided by 1000 (i.e. removed three trailing zeros)
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    NSLog(@"formattedDateString: %@", formattedDateString);
    return formattedDateString;
}
-(NSString*)convertDateSheetInfo:(NSString*)inputDateString{
//    NSString *trimmedString = [inputDateString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //    NSString *string1=[inputDateString stringByAppendingString:@" 00:00:00 +0000"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date1 = [dateFormatter dateFromString:inputDateString];
    [dateFormatter setDateFormat:@"yyyy-MM-dd "];
    return [dateFormatter stringFromDate:date1];
}
-(NSString *)convertDateFromDate:(NSDate *) timeDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    NSString *formattedDateString = [dateFormatter stringFromDate:timeDate];
    NSLog(@"formattedDateString: %@", formattedDateString);
    return formattedDateString;
}

#pragma mark SQLite DB

+ (void)setDidCopyDb{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:SETTINGS_DB_COPIED_STATUS];
    
    [defaults synchronize];
}

+ (BOOL)isDbCopied{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults boolForKey:SETTINGS_DB_COPIED_STATUS];
}



+ (NSString*)getDbPath{
    NSString *dbPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    return [dbPath stringByAppendingPathComponent:@"DB/PartyEDU.sqlite"];
}

+ (void)copyDbToPath{
    if ([self isDbCopied]) {
        return;
    }
    
    
    //Using NSFileManager we can perform many file system operations.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // get shared file manager object
    NSString *dbPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    BOOL isDir = NO;
    NSError *error = nil;
    
    // create path at: APP-ID/Library/DB
    dbPath = [dbPath stringByAppendingPathComponent:@"DB"];
    if (![fileManager fileExistsAtPath:dbPath isDirectory:&isDir] && isDir == NO) {
        if(![fileManager createDirectoryAtPath:dbPath withIntermediateDirectories:YES attributes:nil error:&error]){
            NSLog(@"Failed to create eJournalDataDirectory. Error: %@", error);
            return;
        }
    }
    dbPath = [dbPath stringByAppendingPathComponent:DATABASE_FILE_NAME];
    
    // check if sql-file existing
    if(![fileManager fileExistsAtPath:dbPath]) {
        // no db existing, do copy new db
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATABASE_FILE_NAME];
        
        if (![fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error]) {
            // cannot copy new db to location
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        }
    }
    
    // save settings that tell application don't copy new db
    [self setDidCopyDb];
}


@end
