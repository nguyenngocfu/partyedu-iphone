//
//  PERegisterViewController.h
//  PartyEDU
//
//  Created by thinhpham on 12/27/14.
//  Copyright (c) 2014 com.vn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCustomTextfield.h"
@interface PERegisterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewRegister;
@property (weak, nonatomic) IBOutlet MCustomTextfield *nameTextField;
@property (weak, nonatomic) IBOutlet MCustomTextfield *emailTextField;
@property (weak, nonatomic) IBOutlet MCustomTextfield *passTextField;
@property (weak, nonatomic) IBOutlet MCustomTextfield *rePassTextField;
@property (weak, nonatomic) IBOutlet UIButton  *termOfUseButton;
@property (weak, nonatomic) IBOutlet UIButton *buttonAccept;
- (IBAction)buttonAcceptClick:(id)sender;
- (IBAction)buttonRegisterClick:(id)sender;

@end
