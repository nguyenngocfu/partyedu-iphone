//
//  PESearchResultViewController.m
//  PartyEDU
//
//  Created by thinhpham on 2/17/15.
//  Copyright (c) 2015 com.vn. All rights reserved.
//

#import "PESearchResultViewController.h"
#import "UserModel.h"
#import "AFNetworking.h"
#import "NSObject+SBJson.h"
#import "AFNetworking.h"
#import "Appclient.h"
#import "MFSideMenu.h"
#import "PECustomFriendCell.h"
#import "PEPartyViewController.h"
#import "PEDetailFriendViewController.h"

@interface PESearchResultViewController (){
    NSMutableArray *arrayFriend;
}

@end

@implementation PESearchResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        arrayFriend = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    
}
- (void)viewWillAppear:(BOOL)animated{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getListSearchResult:(NSString *)keyWord{
    
    if (![[Helpers shared]checkNetworkAvailable]) {
        [[Helpers shared] alertStatus:NO_INTERNET_CONECTION title:@"" delegate:nil];
        
        return;
    }
    [arrayFriend removeAllObjects];
    MBProgressShowing();
    AppClient *client = [[AppClient alloc]initJSONClient];
    [client searchFriendWithUserID:(int)[[Helpers shared] getIntegerForKey:USER_ID] andKeyWord:keyWord success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *results = [responseString JSONValue];
        MBProgressHiding();
        if ([[results objectForKey:@"Status"] isEqualToString:@"000"]) {
            NSArray *eventArr = [results objectForKey:@"details"];
            for (NSDictionary *dict in eventArr) {
                UserModel *friend = [[UserModel alloc]initWithDictionary:dict];
                [arrayFriend addObject:friend];
            }
            [_tableFriend reloadData];
            
        }else if ([[results objectForKey:@"Status"] isEqualToString:@"010"]) {
            [[Helpers shared] alertStatus:@"No results are available"title:@"" delegate:nil];
        }
//        else {
//            [[Helpers shared] alertStatus:[results objectForKey:@"Message"]title:@"" delegate:nil];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@",error);
        MBProgressHiding();
    }];
    
}


#pragma mark Custom NavigationBar

- (UIBarButtonItem *)leftMenuBarButtonItem {
    // add menu button
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    menuButton.frame = CGRectMake(0, 0, 25, 30);
    [menuButton setTitle:@"" forState:UIControlStateNormal];
    [menuButton setImage:[UIImage imageNamed:@"button_Menu.png"] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(leftSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:menuButton];
}
- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    // add menu button
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 25, 30);
    [rightButton setTitle:@"" forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"icon_Location.png"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}
- (void)rightSideMenuButtonPressed:(id)sender {
    // Push to Myparties view
    PESideMenuContainerViewController *slideMenu = [myAppDelegate slideMenu];
    [[slideMenu centerViewController] popToRootViewControllerAnimated:NO];
    PEPartyViewController *parties = [[PEPartyViewController alloc]initWithNibName:@"PEPartyViewController" bundle:nil];
    [parties getCurrentLocation];
    [[slideMenu centerViewController] pushViewController:parties animated:NO];
}

#pragma -mark TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayFriend.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIndentifier = @"PECustomFriendCell";
    PECustomFriendCell *cell = (PECustomFriendCell*)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (cell == nil) {
        cell =  [[[NSBundle mainBundle] loadNibNamed:@"PECustomFriendCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.titleUserName.text = [NSString stringWithFormat:@"%@ %@",[[arrayFriend objectAtIndex:indexPath.row]firstName],[[arrayFriend objectAtIndex:indexPath.row]lastName]];
    [cell.img_avarta setImageWithURL:[NSURL URLWithString:[[arrayFriend objectAtIndex:indexPath.row]avataLink]] placeholderImage:[UIImage imageNamed:@"avatar.png"]];
    cell.favoriteDrinkLabel.text = [[arrayFriend objectAtIndex:indexPath.row]sortDescription];
    cell.collegeLabel.text = [[arrayFriend objectAtIndex:indexPath.row]college];
    cell.titleUserName.frame = CGRectMake(cell.titleUserName.frame.origin.x, cell.titleUserName.frame.origin.y, 230, cell.titleUserName.frame.size.height);
    cell.favoriteDrinkLabel.frame = CGRectMake(cell.favoriteDrinkLabel.frame.origin.x, cell.favoriteDrinkLabel.frame.origin.y, 230, cell.favoriteDrinkLabel.frame.size.height);
    cell.collegeLabel.frame = CGRectMake(cell.collegeLabel.frame.origin.x, cell.collegeLabel.frame.origin.y, 230, cell.collegeLabel.frame.size.height);
    cell.buttonInvite.hidden = YES;
    cell.buttonMessage.hidden = YES;
    cell.buttonAccept.hidden = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PEDetailFriendViewController *detailFriend = [[PEDetailFriendViewController alloc] initWithNibName:@"PEDetailFriendViewController" bundle:nil];
    detailFriend.userInfo = [arrayFriend objectAtIndex:indexPath.row];
    detailFriend.checkFriend = 0;
    [self.navigationController pushViewController:detailFriend animated:YES];
}

@end
